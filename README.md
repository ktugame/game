# **2D Shooter** #
---
### -The download link was [here](https://kovenas.itch.io/salayc)- ###

### Better check from [this commit](https://bitbucket.org/ktugame/game/src/4ce3179e0423?at=master) ###

---
## **What libraries do we use?** ##

# Now #

* [SFML for sounds and OpenGL stuff](https://www.sfml-dev.org/)

# Future #

* MAYBE: [Steamworks api](https://steamcommunity.com/dev)
* MAYBE: FreeImage for loading image files
* MAYBE: We'll remove SFML library from our code

---
## **What programs or apps do we use?** ##

* [Visual Studio Community Edition](https://www.visualstudio.com/downloads/)
* [Source Tree](https://www.sourcetreeapp.com/)

---
## **Installation and Configuration** ##

* [Download SFML library from our Google Drive]( https://drive.google.com/drive/folders/0B92h94WtJMdJMjh6Z2dyQjUwV00?usp=sharing )
* Extract SFML-2.4.2-windows-vc14-32-bit.zip file
* In the **\\GameResp\\Engine\\** directory create folder with name **SFML-2.4.2-32bit**
* Copy and paste all **SFML** files from extracted folder to our created folder **\\GameResp\\Engine\\SFML-2.4.2-32bit\\** 
* Open **Engine.sln** and **Launch the game**

**You're ready to Go!**

---
## **Our team** ##

* Aleksas Golubevas
* Gabrielius Tiknius
* Mantas Kleiva
* Tomas Zigmantavičius
* Giedrius Burokas

---
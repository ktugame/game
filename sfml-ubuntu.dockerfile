# Ubuntu latest
FROM ubuntu:latest

# Update & upgrade
RUN apt-get update -y \
 && apt-get upgrade -y

# Install stuff
RUN apt-get install -y \
 g++ build-essential freeglut3 \
 aptitude libxi-dev libxmu-dev \
 libx11-dev libxcb1-dev libx11-xcb-dev \
 libxcb-randr0-dev libxcb-image0-dev \
 libgl1-mesa-dev libudev-dev \
 libfreetype6-dev libjpeg-dev \
 libopenal-dev libflac-dev libvorbis-dev \
 libsfml-dev dos2unix git wget cmake

RUN apt-get autoremove

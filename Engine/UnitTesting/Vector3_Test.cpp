#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <Vector3.h>

namespace UnitTesting
{
	TEST_CLASS(Vector3_Test)
	{
	public:

		TEST_METHOD(Vector3_C_Initialization)
		{
			Vector3 vec(0, 0, 0);
			Assert::AreEqual(0.0f, vec.x);
			Assert::AreEqual(0.0f, vec.y);
			Assert::AreEqual(0.0f, vec.z);

			vec = Vector3(1, 2, 3);
			Assert::AreEqual(1.0f, vec.x);
			Assert::AreEqual(2.0f, vec.y);
			Assert::AreEqual(3.0f, vec.z);
		}

		TEST_METHOD(Vector3_C_Operator_Add)
		{
			Vector3 v1(20, 0, -5);
			Vector3 v2(0, 10, 0);

			Assert::AreEqual(20.0f, (v1 + v2).x);
			Assert::AreEqual(10.0f, (v1 + v2).y);
			Assert::AreEqual(-5.0f, (v1 + v2).z);
		}

		TEST_METHOD(Vector3_C_Operator_Minus)
		{
			Vector3 v1(10, 0, -5);
			Vector3 v2(10, 5, 0);

			Assert::AreEqual(0.0f, (v1 - v2).x);
			Assert::AreEqual(-5.0f, (v1 - v2).y);
			Assert::AreEqual(-5.0f, (v1 - v2).z);
		}

		TEST_METHOD(Vector3_C_Operator_Multiply)
		{
			Vector3 v(-10, 5, 100);

			Assert::AreEqual(-20.0f, (v * 2).x);
			Assert::AreEqual(10.0f, (v * 2).y);
			Assert::AreEqual(200.0f, (v * 2).z);
		}

		TEST_METHOD(Vector3_C_Operator_Divide)
		{
			Vector3 v(4, -10, 100);

			Assert::AreEqual(2.0f, (v / 2).x);
			Assert::AreEqual(-5.0f, (v / 2).y);
			Assert::AreEqual(50.0f, (v / 2).z);
		}

	};
}

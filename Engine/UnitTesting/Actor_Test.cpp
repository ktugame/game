#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <Actor.h>

namespace UnitTesting
{
	TEST_CLASS(Actor_Test)
	{
	public:

		TEST_METHOD(Actor_IsAlive)
		{
			Actor *actor = new Actor();

			Assert::AreEqual(true, actor->IsAlive());

			delete actor;
		}

		TEST_METHOD(Actor_HealthAndDamage)
		{
			Actor *actor = new Actor();

			int health = actor->GetHealth();
			actor->TakeDamage(10);
			Assert::AreEqual(health - 10, actor->GetHealth());

			delete actor;
		}

		TEST_METHOD(Actor_Death)
		{
			Actor *actor = new Actor();

			int health = actor->GetHealth();
			actor->TakeDamage(health);
			Assert::IsFalse(actor->IsAlive());

			delete actor;
		}

		TEST_METHOD(Actor_Position)
		{
			Actor *actor = new Actor();

			Assert::IsTrue(sf::Vector2f(0, 0) == actor->GetPosition());

			delete actor;
		}

	};
}

#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <Enemy.h>

namespace UnitTesting
{
	TEST_CLASS(Actor_Test)
	{
	public:

		TEST_METHOD(Enemy_IsAlive)
		{
			Enemy *enemy = new Enemy({0,0}, 20);

			Assert::AreEqual(true, enemy->IsAlive());

			delete enemy;
		}

		TEST_METHOD(Enemy_Health)
		{
			Enemy *enemy = new Enemy({ 0,0 }, 20);

			int health = enemy->GetHealth();
			enemy->TakeDamage(10);
			Assert::AreEqual(health - 10, enemy->GetHealth());

			delete enemy;
		}

		TEST_METHOD(Enemy_Damage)
		{
			Enemy *enemy = new Enemy({ 0,0 }, 20);

			Assert::IsTrue(20 == enemy->Attack());

			delete enemy;
		}

		TEST_METHOD(Enemy_InRange)
		{
			Enemy *enemy = new Enemy({ 0,0 }, 20);

			Assert::IsTrue(enemy->InRange({ 5,5 }));
			Assert::IsTrue(enemy->InRange({ -5,-5 }));
			Assert::IsFalse(enemy->InRange({ 15, 15 }));
			Assert::IsFalse(enemy->InRange({-15, -15}));

			delete enemy;
		}

		TEST_METHOD(Enemy_Death)
		{
			Enemy *enemy = new Enemy({ 0,0 }, 20);

			int health = enemy->GetHealth();
			enemy->TakeDamage(health);
			Assert::IsFalse(enemy->IsAlive());

			delete enemy;
		}

		TEST_METHOD(Enemy_Position)
		{
			Enemy *enemy = new Enemy({ 10,10 }, 20);

			Assert::IsTrue(sf::Vector2f(10,10) == enemy->GetPosition());

			delete enemy;
		}

	};
}

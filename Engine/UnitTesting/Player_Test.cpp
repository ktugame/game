#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <Player.h>

namespace UnitTesting
{
	TEST_CLASS(Player_Test)
	{
	public:

		TEST_METHOD(Player_IsAlive)
		{
			Player *player = new Player({0,0});

			Assert::AreEqual(true, player->IsAlive());

			delete player;
		}

		TEST_METHOD(Player_HealthAndDamage)
		{
			Player *player = new Player({ 0,0 });

			int health = player->GetHealth();
			player->TakeDamage(10);

			Assert::AreEqual(health - 10, player->GetHealth());
		}

		TEST_METHOD(Player_Death)
		{
			Player *player = new Player({ 0,0 });

			int health = player->GetHealth();
			player->TakeDamage(health);

			Assert::IsFalse(player->IsAlive());
		}

		TEST_METHOD(Player_Position)
		{
			Player *player = new Player({ 10,10 });

			Assert::IsTrue(sf::Vector2f(10,10) == player->GetPosition());
		}

	};
}

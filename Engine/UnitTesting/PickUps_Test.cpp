#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <PickUp.h>
#include <PHealth.h>
#include <PSpeed.h>
#include <PWeapon.h>
#include <Weapon.h>

namespace UnitTesting
{
	TEST_CLASS(Pickup_Test)
	{
	public:

		TEST_METHOD(Pickup_Initialization)
		{
			PickUp *pickup = new PickUp({ 0,0 }, 5.0f);

			Assert::IsTrue(sf::Vector2f(0, 0) == pickup->GetPosition());
			Assert::IsTrue(PickUp::PICKUPTYPE::pNone == pickup->GetType());
			Assert::IsTrue(5.0f == pickup->LeftLivingTime());

			delete pickup;
		}

		TEST_METHOD(Pickup_DestroyState)
		{
			PickUp *pickup = new PickUp({ 0,0 }, 5.0f);

			Assert::AreEqual(false, pickup->DestroyState());

			pickup->DestroyObject();
			Assert::AreEqual(true, pickup->DestroyState());

			delete pickup;
		}

		TEST_METHOD(Pickup_Speed1)
		{
			PickUp *pickup = new PSpeed({ 0,0 }, 0);

			Assert::IsTrue(pickup != nullptr);

			float speed = static_cast<PSpeed*>(pickup)->Speed();
			Assert::IsTrue(speed > 0);

			float duration = static_cast<PSpeed*>(pickup)->Duration();
			Assert::IsTrue(duration > 0);

			delete pickup;
		}

		TEST_METHOD(Pickup_Health1)
		{
			PickUp *pickup = new PHealth({ 0,0 }, 0);

			Assert::IsTrue(pickup != nullptr);

			int hp = static_cast<PHealth*>(pickup)->HealthAmount();
			Assert::IsTrue(hp > 0);

			delete pickup;
		}

		TEST_METHOD(Pickup_Weapon1)
		{
			PickUp *pickup = new PWeapon({ 0,0 }, 0);

			Assert::IsTrue(pickup != nullptr);
			Weapon *wep = static_cast<PWeapon*>(pickup)->NewWeapon();
			Assert::IsTrue(wep != nullptr);

			int ammo = wep->GetAmmoPerClip();
			wep->Shoot();
			Assert::AreEqual(ammo - 1, wep->GetCurrentAmmo());

			wep->Reload();
			Assert::AreEqual(ammo, wep->GetCurrentAmmo());

			delete pickup;
			delete wep;
		}
	};
}

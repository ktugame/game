#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <Widget.h>

namespace UnitTesting
{
	TEST_CLASS(Widget_Test)
	{
	public:

		TEST_METHOD(Widget_ToggleVisibility)
		{
			Widget *wid = new Widget();

			Assert::AreEqual(true, wid->IsVisible());

			wid->ToggleVisibility(); 
			Assert::AreEqual(false, wid->IsVisible());

			delete wid;
		}

		TEST_METHOD(Widget_DestroyWidget)
		{
			Widget *wid = new Widget();

			wid->DestoyWidget();
			Assert::AreEqual(true, wid->Destroy());

			delete wid;
		}

		TEST_METHOD(Widget_Hide_Show)
		{
			Widget *wid = new Widget();

			wid->Hide();
			Assert::AreEqual(false, wid->IsVisible());

			wid->Show();
			Assert::AreEqual(true, wid->IsVisible());

			delete wid;
		}
	};
}

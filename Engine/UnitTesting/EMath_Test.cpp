#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <EMath.h>

namespace UnitTesting
{
	TEST_CLASS(EMath_Test)
	{
	public:

		TEST_METHOD(EMath_VectorLength)
		{
			sf::Vector2f v1(4.0f, 3.0f);

			Assert::AreEqual(5.0f, Math::GetLength(v1));
		}

		TEST_METHOD(EMath_Distance)
		{
			sf::Vector2f v1(1.0f, 1.0f);
			sf::Vector2f v2(2.0f, 2.0f);

			Assert::AreEqual(static_cast<float>(sqrt(2)), Math::Distance(v1, v2));
		}

		TEST_METHOD(EMath_Normalize)
		{
			sf::Vector2f v1(1.0f, 1.0f);

			float resultX = v1.x / static_cast<float>(sqrt(v1.x + v1.y));
			float resultY = v1.y / static_cast<float>(sqrt(v1.x + v1.y));

			Assert::AreEqual(resultX, Math::Normalize(v1).x);
			Assert::AreEqual(resultY, Math::Normalize(v1).y);
		}

		TEST_METHOD(EMath_Random1)
		{
			int x = -1;

			for (int i = 0; i < 100; i++)
			{
				x = Math::Random(0, 5);
				Assert::IsTrue(x >= 0 && x <= 5);
			}


			x = Math::Random(11, 11);
			Assert::IsTrue(x == 11);


			x = Math::Random(14, 13);
			Assert::IsTrue(x >= 13 && x <= 14);


			x = Math::Random(100, 101);
			Assert::IsTrue(x >= 100 && x <= 101);

		}

		TEST_METHOD(EMath_Random2)
		{
			float x = -1.0f;

			for (int i = 0; i < 100; i++)
			{
				x = Math::Random(2.4444f, 3.555f);
				Assert::IsTrue(x >= 2.4444f && x <= 3.555f);
			}


			x = Math::Random(11.0f, 11.0f);
			Assert::IsTrue(x == 11.0f);


			x = Math::Random(14.2f, 13.0f);
			Assert::IsTrue(x >= 13.0f && x <= 14.2f);


			x = Math::Random(100.3f, 101.0f);
			Assert::IsTrue(x >= 100.3f && x <= 101.0f);

		}

		TEST_METHOD(EMath_Random3)
		{
			sf::Vector2f x(0, 0);

			for (int i = 0; i < 100; i++)
			{
				x = Math::Random(sf::Vector2f(-1.0f, -1.0f), sf::Vector2f(1.0f, 1.0f));
				Assert::IsTrue(x.x >= -1.0f && x.x <= 1.0f);
				Assert::IsTrue(x.y >= -1.0f && x.y <= 1.0f);
			}


			x = Math::Random(sf::Vector2f(11.0f, 11.0f), sf::Vector2f(11.0f, 11.0f));
			Assert::IsTrue(x.x == 11.0f);
			Assert::IsTrue(x.y == 11.0f);


			x = Math::Random(sf::Vector2f(14.0f, 14.0f), sf::Vector2f(13.0f, 13.0f));
			Assert::IsTrue(x.x >= 13.0f && x.x <= 14.0f);
			Assert::IsTrue(x.y >= 13.0f && x.y <= 14.0f);


			x = Math::Random(sf::Vector2f(100.0f, 100.0f), sf::Vector2f(100.1f, 100.1f));
			Assert::IsTrue(x.x >= 100.0f && x.x <= 100.1f);
			Assert::IsTrue(x.y >= 100.0f && x.y <= 100.1f);

		}

	};
}

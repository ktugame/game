#pragma once

#include "PickUp.h"

class Weapon;
class GameEngine;

class PWeapon : public PickUp
{
private:

	// Weapon
	Weapon *weapon;

	// Generate random weapon
	inline void GenerateWeapon();

	// Is the weapon taken
	bool IsTaken;

	GameEngine *Engine;

public:

#ifndef TESTING

	// Constructor
	// Engine pointer
	// Player pointer
	// Entity position
	// Living time
	PWeapon(GameEngine *Engine, Player **GamePlayer,
		sf::Vector2f EntityPosition, float LiveTimer);

#else

	// Constructor for testing
	PWeapon(sf::Vector2f EntityPosition, float LiveTimer);

#endif

	// Destructor
	~PWeapon();

	// Get weapon
	Weapon *NewWeapon();

};

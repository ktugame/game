﻿#pragma once

#ifndef TESTING
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#endif

class Widget;
class UIText;
class GameEngine;
class SoundManager;
class ContentLoader;

// WEAPON class for random guns/weapons
class Weapon : public sf::Sprite
{
protected:

	// GameEngine pointer
	GameEngine *Engine;

	// Weapon widget
	//Widget *WeaponWidget;

	// Text for displaying ammo and reload / fire timers
	//UIText *uiAmmoText, *uiReloadTimer, *uiFireTimer;

	// Current ammo / max ammo / ammo per clip
	int AmmoCurrent, AmmoPerClip;

	// Guns damage
	int Damage;

	// Fire/shooting speed
	float FireSpeed;

	// Reload speed
	float ReloadSpeed;

	// Gun's range (meters?)
	float Range;

	// Gun's accuracy
	// 100% means 'no recoil'
	float Accuracy;

	// Gun is automatic ?
	bool bAutomatic;

	// If we shot from gun?
	// This boolean for non-automatic guns
	bool bShooted;

	// Tikriname, ar neuzsitaisinejame ginklo
	bool bReloading;

	// Fire timer
	float FireTimer;

	// Reload timer
	float ReloadTimer;



public:

	// Consturctor
	// Engine pointer
	// Bullet list pointer
	// Gun properties
	Weapon(GameEngine *Engine, unsigned int AmmoPerClip, unsigned int Damage,
		float FireSpeed, float ReloadSpeed, float Range, float Accuracy,
		bool bAutomatic);

	// Update position and other stuff
	virtual void Update(const float Tick, float Rotation, sf::Vector2f Position);

	// Get sprite
	virtual sf::Sprite *GetSprite();

	// Destructor
	~Weapon();

	// Shoot
	virtual void Shoot();

	// Reload gun
	virtual void Reload();

	// Get how many bullets left in the clip
	int GetCurrentAmmo() const;

	// Get ammo per clip number
	int GetAmmoPerClip() const;

	// Is gun automatic?
	bool IsAutomatic() const;

protected:

	// Skaiciuoja, kiek ammo yra
	virtual void CalculateAmmo();

	// Patikriname, ar turime kulku apkaboje ka saudyti
	bool CheckWeaponStatus();

};

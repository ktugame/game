#include "ParticleSystem.h"
#include "../Math/EMath.h"

ListParticle ParticleSystem::StaticParticleList;

ParticleSystem::ParticleSystem(uint32 Count, sf::Vector2f Position, sf::Color Color)
	: Particles(Count),
	Vertices(sf::Points, Count),
	LifeTime(Math::Random(0.1f, 0.3f)),
	Position(Position)
{
	StaticParticleList.push_back(this);
	bIsDone = false;

	this->Color = Color;

	for (uint32 i = 0; i < Particles.size(); ++i)
	{
		resetParticle(i);
	}
}

ParticleSystem::~ParticleSystem()
{

}

void ParticleSystem::Update(const float Time)
{
	if (bIsDone) return;

	bool Done = true;

	for (uint32 i = 0; i < Particles.size(); ++i)
	{
		// update the particle lifetime
		Particle& p = Particles[i];
		p.LifeTime -= Time;

		if (p.LifeTime > 0)
			Done = false;

		// update the position of the corresponding vertex
		Vertices[i].position += p.velocity * Time;

		// update the alpha (transparency) of the particle according to its lifetime
		float ratio = p.LifeTime / LifeTime;
		Vertices[i].color = Color;
		Vertices[i].color.a = static_cast<sf::Uint8>(ratio * 255);
	}

	bIsDone = Done;
}

void ParticleSystem::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	// apply the transform
	states.transform *= getTransform();

	// our particles don't use a texture
	states.texture = NULL;

	// draw the vertex array
	target.draw(Vertices, states);
}

void ParticleSystem::resetParticle(std::size_t index)
{
	// give a random velocity and lifetime to the particle
	float angle = Math::Random(0.0f, 360.0f) * CONST_DEG_TO_RADIANS;
	float speed = Math::Random(300.0f, 500.0f);
	Particles[index].velocity = sf::Vector2f(std::cos(angle) * speed, std::sin(angle) * speed);
	Particles[index].LifeTime = Math::Random(0.1f, 0.3f);

	// reset the position of the corresponding vertex
	Vertices[index].position = Position;
}

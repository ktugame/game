#pragma once

#include "Weapon.h"

// Shotgun - Model: WMossberg590
// https://en.wikipedia.org/wiki/Mossberg_500
class WMossberg590 : public Weapon
{
public:

	// Consturctor
	// Engine pointer
	// Player pointer
	WMossberg590(GameEngine *Engine);

	// Destructor
	~WMossberg590();

	// Shooting
	void Shoot();

	// Reloading
	void Reload();

};

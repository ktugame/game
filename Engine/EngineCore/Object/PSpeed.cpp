#include "PSpeed.h"
#include "../Math/EMath.h"

#ifndef TESTING

PSpeed::PSpeed(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, float LiveTimer)
	: PickUp(Engine, GamePlayer, EntityPosition, LiveTimer)
{
	this->Type = pSpeed;
	this->Animated = true;

	this->setTextureRect({ 64 * 0, 64 * 1, 64 * 1, 64 * 1 });

	Anim.FrameCount = 3;
	Anim.Position = sf::Vector2i(0, 1);
	Anim.Size = sf::Vector2f(64, 64);
	Anim.Speed = 0.133f;
}

#else

PSpeed::PSpeed(sf::Vector2f EntityPosition, float LiveTimer)
	: PickUp(EntityPosition, LiveTimer)
{
	this->Type = pSpeed;
}

#endif

PSpeed::~PSpeed()
{

}

float PSpeed::Speed() const
{
	return Math::Random(1.5f, 2.3f);
}

float PSpeed::Duration() const
{
	return Math::Random(4.0f, 6.0f);
}

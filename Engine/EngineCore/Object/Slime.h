#pragma once

#include "Enemy.h"

class Slime : public Enemy
{
private:

	// Slime scale
	float Scale;

public:
	Slime(Player **GamePlayer, GameEngine *Engine, sf::Vector2f SpawnPosition, float Scale = 1.0f);

	~Slime();

	void TakeDamage(int Damage);
};

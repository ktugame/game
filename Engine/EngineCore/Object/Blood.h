#pragma once
#include "Entity.h"

class Blood;
class GameEngine;

using ListBloodSplashes = std::vector<Blood>;

class Blood : public Entity
{
protected:

	// Living timer
	float LiveTimer;

	// List of aviable sprites
	std::vector<char const*> bloodSprites = {
		"Blood00",
		"Blood01",
		"Blood02",
		"Blood03",
		"Blood04",
		"Blood05",
		"Blood06",
	};

	bool Destroy;

public:

	// Constructor
	// Engine pointer
	// Player Pointer pointer
	// Entity position
	Blood(GameEngine *Engine, sf::Vector2f EntityPosition);

	~Blood();

	//update function
	void Update(const float Tick);

	// Gets object state
	bool DestroyState();

	// Destroy blood splash (command)
	void DestroyObject();

	// Get blood splash position
	sf::Vector2f GetPosition();

	// blood splash list
	static ListBloodSplashes StaticBloodList;

	// Get living time
	float LeftLivingTime();

};


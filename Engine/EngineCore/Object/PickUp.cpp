#include "PickUp.h"
#include "Player.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"

ListPickUps PickUp::StaticPickupList;

PickUp::PickUp(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, float LiveTimer)
	//: Entity(Engine, EntityPosition)
{
	//this->setTexture(*Engine->GameContent->GetTextures()["Pickup"]);
	this->setTextureRect({ 64 * 0, 64 * 0, 64 * 1, 64 * 1 });
	this->setOrigin({ 32, 32 });
	this->GamePlayer = GamePlayer;
	this->LiveTimer = LiveTimer;
	this->Collision = false;
	this->Animated = false;
	this->Destroy = false;
	this->Type = pNone;
}

void PickUp::Update(const float Tick)
{
	// pickup is destroyed if pickup existed longer than it's life time
	if (0 > LiveTimer)
	{
		this->Destroy = true;
		return;
	}

	LiveTimer -= Tick;

	if (Animated)
	{
		if (Anim.SpeedTimer >= Anim.Speed)
		{
			Anim.CurrentFrame++;

			if (Anim.CurrentFrame > Anim.FrameCount - 1) Anim.CurrentFrame = 0;

			this->setTextureRect({
				(Anim.CurrentFrame + Anim.Position.x) * static_cast<int>(Anim.Size.x),
				Anim.Position.y * static_cast<int>(Anim.Size.y),
				static_cast<int>(Anim.Size.x),
				static_cast<int>(Anim.Size.y)
			});

			Anim.SpeedTimer = 0;
		}

		Anim.SpeedTimer += Tick;
	}
}

PickUp::~PickUp()
{

}

float PickUp::LeftLivingTime()
{
	return LiveTimer;
}

PickUp::PICKUPTYPE PickUp::GetType()
{
	return this->Type;
}

bool PickUp::GetCollision()
{
	return Collision;
}

bool PickUp::DestroyState()
{
	return this->Destroy;
}

void PickUp::DestroyObject()
{
	this->Destroy = true;
}

sf::Vector2f PickUp::GetPosition()
{
	return this->getPosition();
}

#include "Blood.h"
#include "Player.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "../Math/EMath.h"

ListBloodSplashes Blood::StaticBloodList;

Blood::Blood(GameEngine *Engine, sf::Vector2f EntityPosition)
	//: Entity(Engine, EntityPosition)
{
	//this->setTexture(
	//	*Engine->GameContent->GetTextures()
	//	[bloodSprites[Math::Random(0, static_cast<int>(bloodSprites.size()) - 1)]]
	//);
	this->setOrigin({ 32, 32 });
	this->LiveTimer = 45.0f;
	this->Destroy = false;
}

Blood::~Blood()
{
}

void Blood::Update(const float Tick)
{
	// blood splashe is destroyed if it existed longer than it's life time
	if (0 > LiveTimer)
	{
		this->Destroy = true;
		return;
	}

	LiveTimer -= Tick;
}

bool Blood::DestroyState()
{
	return this->Destroy;
}

void Blood::DestroyObject()
{
	this->Destroy = true;
}

sf::Vector2f Blood::GetPosition()
{
	return this->getPosition();
}

float Blood::LeftLivingTime()
{
	return LiveTimer;
}

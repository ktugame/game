#ifndef ENEMYSPAWNER_H
#define ENEMYSPAWNER_H
#pragma once

#include "Entity.h"

class Player;
class GameEngine;
class EnemySpawner;

using uint = unsigned int;

using ListEnemySpawners = std::vector<EnemySpawner*>;

//To spawn enemies from specified position
class EnemySpawner : public Entity
{
private:

	// TODO: Add comments
	// Current spawning time
	float CurrTimer;

	// Spawn timer
	float SpawningTimer;

	// Max Health of the spawner
	int Health;

	// Engine pointer
	GameEngine *Engine;

	// Spawner state'as
	bool destroy;

	// Spawner alive state
	bool bAlive;

	// Player pointer
	Player **GamePlayer;

public:

	// Constructor
	// Engine pointer
	// Game player pointer
	// Entity positon
	// Enemy list
	EnemySpawner(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, unsigned int health);

	// Destructor
	~EnemySpawner();

	// Update position and other stuff
	void Update(const float Tick);


	// Gets object state
	bool DestroyState();

	// Actor takes damage. dmg - amount of damage
	// returns if spawner is dead
	bool TakeDamage(int dmg);

	//Check if object is in range with enemy
	bool InRange(sf::Vector2f vec);

	// List of spawners
	static ListEnemySpawners StaticSpawnerList;

};

#endif

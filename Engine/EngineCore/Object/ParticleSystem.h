#pragma once

#include <SFML/Graphics.hpp>
#include <vector>

class ParticleSystem;

using ListParticle = std::vector<ParticleSystem*>;

using uint32 = unsigned int;

class ParticleSystem : public sf::Drawable, public sf::Transformable
{
private:

	// Emiter position
	sf::Vector2f Position;

	// Particle
	struct Particle
	{
		sf::Vector2f velocity;
		float LifeTime;
	};

	// Life time
	float LifeTime;

	// Particle list
	std::vector<Particle> Particles;

	// Vertices
	sf::VertexArray Vertices;

	// Is done
	bool bIsDone;

	sf::Color Color;

public:

	// Constructor
	ParticleSystem(uint32 Count, sf::Vector2f Position,
		sf::Color Color = sf::Color::Red);

	// Destructor
	~ParticleSystem();

	// Update
	void Update(const float Time);

	// Particle list
	static ListParticle StaticParticleList;

	// Check particle system status
	bool IsDone() { return bIsDone; }

private:

	// SFML draw method
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void resetParticle(std::size_t index);

};

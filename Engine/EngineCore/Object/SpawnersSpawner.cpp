#include "SpawnersSpawner.h"
#include "EnemySpawner.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "../Math/EMath.h"

SpawnersSpawner::SpawnersSpawner(GameEngine *Engine, Player **GamePlayer)
	//: Entity(Engine, { 0,0 })
{
	this->GamePlayer = GamePlayer;
	this->Engine = Engine;

	this->CurrTimer = 0.0f;
	this->SpawningTimer = Math::Random(0.0f, 2.0f);
}

SpawnersSpawner::~SpawnersSpawner()
{
	this->GamePlayer = nullptr;
}

void SpawnersSpawner::Update(const float Tick)
{
	for (; CurrTimer > SpawningTimer; CurrTimer -= SpawningTimer)
	{
		sf::Vector2f spawnersSpawnerPosition = this->getPosition();
		sf::Vector2f spawnerPosition =
		sf::Vector2f(Math::Random(-2000 + spawnersSpawnerPosition.x, 2000 + spawnersSpawnerPosition.x),
				Math::Random(-2000 + spawnersSpawnerPosition.y, 2000 + spawnersSpawnerPosition.x));
		
		//Setting spawners health (increasing as spawners deathcount increases)
		Health = Math::Random(100, 150)*DeathCount/2;
			
		EnemySpawner::StaticSpawnerList.push_back(new EnemySpawner(Engine, GamePlayer, spawnerPosition, Health));
		
		if(EnemySpawner::StaticSpawnerList.size() >= 1)
			this->SpawningTimer = 20.0f;
		else
			EnemySpawner::StaticSpawnerList.push_back(new EnemySpawner(Engine, GamePlayer, spawnerPosition, Health));
			
	}
	CurrTimer += Tick;
}
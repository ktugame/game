#ifndef PICKUPSPAWNER_H
#define PICKUPSPAWNER_H
#pragma once

#include "Entity.h"
#include "VBullet.h"

class PickUp;
class PHealth;
class PWeapon;
class PSpeed;
class Player;
class GameEngine;

// This class spawns PickUps
class PickUpSpawner : public Entity
{
private:

	// Minimum spawning period
	float MinTime;

	// Maximum spawning period
	float MaxTime;

	// Minimum spawned pickup life time
	float MinPickupLifeTime;

	// Maximum spawned pickup life time
	float MaxPickupLifeTime;

	// Current spawning timer
	float CurrTimer;

	// Spawn timer
	float SpawningTimer;

	// Width and height spawning area
	float SpawnWidth, SpawnHeigth;

	// Player pointer
	Player **GamePlayer;

	// VBullet
	ListBullets *Bullets;

	GameEngine *Engine;

public:

	// Constructor
	// Engine pointer
	// Game player pointer
	// Entity positon
	// Pickup list
	PickUpSpawner(GameEngine *Engine, Player **GamePlayer);

	// Destructor
	~PickUpSpawner();

	// Update position and other stuff
	void Update(const float Tick);

};

#endif // !PICKUPSPAWNER_H

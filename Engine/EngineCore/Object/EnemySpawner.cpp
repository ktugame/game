#include "EnemySpawner.h"
#include "Enemy.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "Bug.h"
#include "Slime.h"
#include "../Math/EMath.h"

ListEnemySpawners EnemySpawner::StaticSpawnerList;

EnemySpawner::EnemySpawner(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, uint Health)
	//: Entity(Engine, EntityPosition)
{
	this->GamePlayer = GamePlayer;
	this->Engine = Engine;

	this->destroy = false;

	this->CurrTimer = 0;
	this->SpawningTimer = Math::Random(5.0f, 8.0f);

	this->setPosition(EntityPosition);
	this->setTextureRect(sf::IntRect(0, 0, 64, 64));
	//this->setTexture(*Engine->GameContent->GetTextures()["EnemySpawner"]);
	this->setOrigin(
		getLocalBounds().width / 2,
		getLocalBounds().height / 2
	);

	sf::Vector2f scale(0, 0);
	scale.x = scale.y = 2;
	this->setScale(scale);
	this->Health = Health;
	this->bAlive = true;
}

EnemySpawner::~EnemySpawner()
{
	this->GamePlayer = nullptr;
}

void EnemySpawner::Update(const float Tick)
{
	if (!destroy)
	{
		for (; CurrTimer > SpawningTimer; CurrTimer -= SpawningTimer)
		{
				switch (Math::Random(0, 1))
				{
				case 1:
				{
					Enemy::StaticEnemyList.push_back(new Bug(GamePlayer, Engine, getPosition()));
				}
				break;

				default:
				{
					Enemy::StaticEnemyList.push_back(new Slime(GamePlayer, Engine, getPosition()));
				}break;

				}
				this->SpawningTimer = Math::Random(5.0f, 8.0f);
		}
		CurrTimer += Tick;
	}
}

bool EnemySpawner::DestroyState()
{
	return this->destroy;
}

bool EnemySpawner::TakeDamage(int dmg)
{
	this->Health -= dmg;

	if (this->Health <= 0)
	{
		this->Health = 0;
		destroy = true;
		bAlive = false;
	}

	return bAlive;
}

/*void EnemySpawner::SetScale()
{
	Enemy::
}*/

bool EnemySpawner::InRange(sf::Vector2f vec)
{
	if (Math::Distance(getPosition(), vec) < getLocalBounds().width / 2.0f)
	{
		return true;
	}
	return false;
}


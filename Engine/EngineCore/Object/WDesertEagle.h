#pragma once

#include "Weapon.h"

// Pistol - Mode: Desert Eagle
// https://en.wikipedia.org/wiki/IMI_Desert_Eagle
class WDesertEagle : public Weapon
{
public:

	// Consturctor
	// Engine pointer
	// Bullet list pointer
	// Player pointer
	WDesertEagle(GameEngine *Engine);

	// Destructor
	~WDesertEagle();

};

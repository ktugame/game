#pragma once

#include "Entity.h"

class PickUp;
class Player;
class GameEngine;

using ListPickUps = std::vector<PickUp*>;

// Animation
struct PIKCUP_ANIMATION_STRUCT
{
	// X = 0, 1, 2, 3 ... n
	// Y = 0, 1, 2, 3 ... n
	sf::Vector2i Position;

	// 64x64, 128x128, 32x32...
	sf::Vector2f Size;

	// Do not modify! (When set)
	// How many X frames...
	int FrameCount;

	// Current frame
	int CurrentFrame;

	// Animation speed (frame / seconds)
	float Speed;

	// Do not modify! (When set)
	// SpeedTimer += Tick only available
	float SpeedTimer;

	// Constructor
	PIKCUP_ANIMATION_STRUCT()
	{
		Position = sf::Vector2i(0, 0);
		Size = sf::Vector2f(0, 0);
		FrameCount = 0;
		CurrentFrame = 0;
		Speed = 0;
		SpeedTimer = 0;
	}
};

class PickUp : public Entity
{
protected:

	// Collision detection
	bool Collision;

	// Living timer
	float LiveTimer;

	// Pickup animated or no
	bool Animated;

	// Animation
	PIKCUP_ANIMATION_STRUCT Anim;

	// Player pointer
	Player **GamePlayer;

	// Destroy status
	bool Destroy;

public:

	// Types of pickups
	enum PICKUPTYPE
	{
		// None pickup (default)
		pNone,

		// Health pickup
		pHealth,

		// Weapon pickup
		pWeapon,

		// Speed pickup
		pSpeed

	};

	// Constructor
	// Engine pointer
	// Entity position
	// Living timer
	PickUp(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, float LiveTimer);

	//update function
	void Update(const float Tick);

	//destructor
	~PickUp();

	// Pickup type
	PICKUPTYPE GetType();

	// Get collision state
	bool GetCollision();

	// Gets object state
	bool DestroyState();

	// Destroy pickup (command)
	void DestroyObject();

	// Get pickup position
	sf::Vector2f GetPosition();

	// Pick up list
	static ListPickUps StaticPickupList;

	// Get living time
	float LeftLivingTime();

protected:

	// Pickup type
	PICKUPTYPE Type;

};

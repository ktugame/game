#include "WMossberg590.h"
#include "VBullet.h"
#include "Player.h"
#include "../Engine/SoundManager.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "../Math/EMath.h"

WMossberg590::WMossberg590(GameEngine *Engine)
	: Weapon(Engine, 2, 20, 0.4f, 1.0f, 500, 0.65f, 1)
{
	//setTexture(*Engine->GameContent->GetTextures()["WMossberg590"]);
	//this->Sprite.setTextureRect({128, 64, 128, 128});
}

WMossberg590::~WMossberg590()
{

}

void WMossberg590::Reload()
{
	if (!bReloading)
	{
		if (AmmoCurrent < AmmoPerClip)
		{
			bReloading = true;
			ReloadTimer = 0;

			//Engine->GameSoundManager->Play("ShotgunPump2");

		}
	}
}

void WMossberg590::Shoot()
{
	if (!this->CheckWeaponStatus())
		return;

	float Acc = (1.0f - Accuracy);
	sf::Vector2f Position = getPosition();
	float Rotation = getRotation() + Math::Random(-25.5f, 25.5f) * Acc;

	VBullet::StaticBulletList.push_back(
		VBullet(Engine, Position, sf::IntRect({ 1, 0 }, { 16, 32 }),
			this->Range, Math::Random(1500.0f, 1700.0f), Rotation, Damage));

	Rotation += Math::Random(-25.5f, 25.5f) * Acc;

	VBullet::StaticBulletList.push_back(
		VBullet(Engine, Position, sf::IntRect({ 1, 0 }, { 16, 32 }),
			this->Range, Math::Random(1600.0f, 1850.0f), Rotation, Damage));

	Rotation += Math::Random(-25.5f, 25.5f) * Acc;

	VBullet::StaticBulletList.push_back(
		VBullet(Engine, Position, sf::IntRect({ 1, 0 }, { 16, 32 }),
			this->Range, Math::Random(1700.0f, 2100.0f), Rotation, Damage));

	//Engine->GameSoundManager->Play("ShotgunShot3");

	bShooted = true;

	AmmoCurrent--;
}

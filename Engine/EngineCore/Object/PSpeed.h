#pragma once

#include "PickUp.h"

class PSpeed : public PickUp
{
public:

#ifndef TESTING

	// Constructor
	// Engine pointer
	// Entity position
	// Living timer
	PSpeed(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, float LiveTimer);

#else

	// Constructor for testing
	PSpeed(sf::Vector2f EntityPosition, float LiveTimer);

#endif

	// Destructor
	~PSpeed();

	// Returns speed
	// Speed multiplicand (dauginamasis)
	float Speed() const;

	// Speed duration
	float Duration() const;


};

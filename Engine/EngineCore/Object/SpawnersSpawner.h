#ifndef SPAWNERSSPAWNER_H
#define SPAWNERSSPAWNER_H
#pragma once

#include "Entity.h"

class Player;
class GameEngine;

class SpawnersSpawner : public Entity
{
private:

	// Current spawning time
	float CurrTimer;

	// Spawn timer
	float SpawningTimer;

	// Spawners health
	int Health;

	// Engine pointer
	GameEngine *Engine;

	// Player pointer
	Player **GamePlayer;


public:

	SpawnersSpawner(GameEngine *Engine, Player **GamePlayer);

	~SpawnersSpawner();

	// Spawners death counter
	int DeathCount = 1;

	// Update position and other stuff
	void Update(const float Tick);
};

#endif
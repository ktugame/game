#pragma once

#include "Enemy.h"

class Bug : public Enemy
{
private:


public:
	Bug(Player **GamePlayer, GameEngine *Engine, sf::Vector2f SpawnPosition);
	~Bug();
};


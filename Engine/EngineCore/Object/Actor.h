#pragma once

#include "Entity.h"

// Actor's max health
constexpr int ACTOR_MaxHealth = 1000;

using uint = unsigned int;

class Actor : public Entity
{
protected:

	// Actor's current health
	int CurrentHealth;

	// Is actor alive?
	bool bIsAlive;

	// Movement speed
	float MovementSpeed;

	// Last position
	sf::Vector2f Position;

public:

	Actor();
	virtual ~Actor();

	// Actor takes damage. dmg - amount of damage.
	// returns if player is dead
	void TakeDamage(uint Damage);

	// Get Actor's position
	sf::Vector2f GetPosition();

	// Gets Actor's state (!bAlive)
	bool IsAlive();

	// Get Actor's health
	int GetHealth();

	// Get Actor's rotation
	float GetRotation();

	// Updates Actor each frame
	virtual void Update(const float Tick);

};

#include "Slime.h"
#include "Enemy.h"
#include "../Engine/ContentLoader.h"
#include "../Engine/GameEngine.h"
#include "VBullet.h"
#include "ParticleSystem.h"
#include "../Math/EMath.h"

Slime::Slime(Player **GamePlayer, GameEngine *Engine, sf::Vector2f SpawnPosition, float ScaleSize)
	: Enemy(GamePlayer, Engine, SpawnPosition)
{
	Scale = ScaleSize;

	//this->setTexture(*Engine->GameContent->GetTextures()["EnemySlime"]);
	this->setOrigin(
		getLocalBounds().width / 2,
		getLocalBounds().height / 2
	);

	this->setScale({ Scale,Scale });

	CurrentHealth = static_cast<int>(15 + 30 * Scale);

	Damage = static_cast<int>(10 + 15 * Scale);

	MovementSpeed = 200.0f;

}

Slime::~Slime()
{
	if (Scale > 0.3f)
	{
		Enemy::StaticEnemyList.push_back(new Slime(GamePlayer, Engine, getPosition() + Math::Random({ -10, -10 }, { 10, 10 }), Scale * 0.5f));
		Enemy::StaticEnemyList.push_back(new Slime(GamePlayer, Engine, getPosition() + Math::Random({ -10, -10 }, { 10, 10 }), Scale * 0.5f));
	}
}

void Slime::TakeDamage(int Damage)
{
	if (!bIsAlive) return;

	new ParticleSystem(200, getPosition(), sf::Color::Green);

	if (CurrentHealth > 0)
		CurrentHealth -= Damage;

	SlowDown();

	if (CurrentHealth <= 0)
	{
		CurrentHealth = 0;
		bIsAlive = false;
		return;
	}
}

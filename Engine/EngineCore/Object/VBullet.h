#ifndef VBULLET_H
#define VBULLET_H
#pragma once

#include <SFML/Graphics.hpp>

class VBullet;
class GameEngine;

using ListBullets = std::vector<VBullet>;

// Class for virtual bullet
class VBullet : public sf::Drawable
{
protected:

	// Sprite for displaying the bullet
	sf::Sprite Sprite;

	// Max travel distance
	float MaxTravelDistance;

	// Bullet travel speed
	float TravelSpeed;

	// Damage
	unsigned int Damage;

	// Rotation
	float Rotation;

	// Angle
	float Angle;

	// Sin and Cos values
	float Sin, Cos;

	// Current travelled distance
	float CurrentTravelledDistance;

	// Ar sunaikinti objekta?
	bool Destroy;

	// Get last position
	sf::Vector2f LastPosition;

public:

	// Constructor
	// Engine pointer
	// Bullet list pointer
	// Spawn position
	// Texture crop rect
	// Travel distance
	// Travel speed
	// Rotation
	// Damage
	VBullet(GameEngine *Engine, sf::Vector2f Position, sf::IntRect Rect,
		float MaxTravelDistance, float TravelSpeed, float Rotation, unsigned int Damage);

	// Destructor
	~VBullet();

	// Update function
	virtual void Update(const float Tick);

	// Should we destroy or not
	bool DestroyState();

	// Get bullet position
	sf::Vector2f GetPosition();

	// Bullets damage
	unsigned int GetDamage();

	// Kulku listas
	static ListBullets StaticBulletList;

private:

	void draw(sf::RenderTarget& Renderer, sf::RenderStates States) const;

};

#endif // VBULLET_H

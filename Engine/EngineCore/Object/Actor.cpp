#include "Actor.h"

Actor::Actor()
{
	CurrentHealth = 100;
	bIsAlive = true;
	MovementSpeed = 200;
}

Actor::~Actor()
{

}

float Actor::GetRotation()
{
	return getRotation();
}

void Actor::Update(const float Tick)
{

}

sf::Vector2f Actor::GetPosition()
{
	return getPosition();
}

bool Actor::IsAlive()
{
	return bIsAlive;
}

int Actor::GetHealth()
{
	return CurrentHealth;
}

void Actor::TakeDamage(uint Damage)
{
	// TODO: this is just ideas, discuss it
	// TODO: create or integrate defence or armor
	// TODO: player armor, level enemies (adds armor or something), etc

	if (CurrentHealth > 0)
		CurrentHealth -= Damage;

	if (CurrentHealth <= 0)
	{
		bIsAlive = false;
		CurrentHealth = 0;
	}
}

#include "Player.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "../Math/EMath.h"

Player::Player(sf::Vector2f spawnPos)
{
	//this->Engine = Engine;
	//this->MousePositionInWorld = &Engine->MousePositionInWorld;

	//sf::Image *img = new sf::Image();
	//img->create(64, 64, sf::Color::Red);
//
	//sf::Texture *tt = new sf::Texture();
	//tt->loadFromImage(*img);
//
	//this->setTexture(*tt);


	//this->setTexture(*Engine->GameContent->GetTextures()["PlayerPistol"]);
	//this->setTextureRect(sf::IntRect(0, 0, 128, 128));
	this->setOrigin(64, 64);
	this->setPosition(spawnPos);

	//this->MovementSpeed = DefaultMovementSpeed;
	//this->currentWeapon = nullptr;
	this->PickupBonuSpeedTimer = 0;
	//this->CurrentHealth = 100;
}

//void Player::AddPickupStatus(PickUp *p)
//{
//	if (!p) return;
//
//	auto Status = p->GetType();
//
//	p->DestroyObject();
//
//	switch (Status)
//	{
//	case PickUp::pNone:
//	{
//		PickUp *Pickup = static_cast<PickUp*>(p);
//
//	}
//	break;
//
//	case PickUp::pHealth:
//	{
//		PHealth *Pickup = static_cast<PHealth*>(p);
//
//		Pickup->HealthAmount();
//
//		int AddHp = Pickup->HealthAmount();
//
//		//CurrentHealth += AddHp;
//
//		//if (CurrentHealth > MaxHealth)
//		//	CurrentHealth = MaxHealth;
//
//	}
//	break;
//
//	case PickUp::pWeapon:
//	{
//		//PWeapon *Pickup = static_cast<PWeapon*>(p);
//
//		//this->GivePlayerWeapon(Pickup->NewWeapon());
//
//	}
//	break;
//
//	case PickUp::pSpeed:
//	{
//		//PSpeed *Pickup = static_cast<PSpeed*>(p);
//
//		//PickupBonuSpeedTimer = Pickup->Duration();
//
//		//MovementSpeed = DefaultMovementSpeed * Pickup->Speed();
//
//	}
//	break;
//
//	default:
//	{
//
//	}
//	break;
//
//	}
//
//}

void Player::Update(const float Tick)
{
//	//if (!bIsAlive) return;
//
//	{
//		// Player movement
//
//		DIR moveX = 0, moveY = 0;
//
//		//if (Engine->IsKeyDown(sf::Keyboard::W)) { moveY--; }
//		//if (Engine->IsKeyDown(sf::Keyboard::A)) { moveX--; }
//		//if (Engine->IsKeyDown(sf::Keyboard::D)) { moveX++; }
//		//if (Engine->IsKeyDown(sf::Keyboard::S)) { moveY++; }
//		//if (Engine->IsMouseButtonDown(sf::Mouse::Left)) { this->Attack(); }
//
//		if (moveX != 0 || moveY != 0)
//		{
//			DIR normalizer = (moveX > 0 ? moveX : (-moveX)) +
//				(moveY > 0 ? moveY : (-moveY));
//
//			float m = 1;
//
//			if (normalizer == 2)
//				m = CONST_1_DIV_SQRT_2;
//
//			float x = 0, y = 0;
//
//			if (moveX != 0)
//				x = static_cast<float>(moveX) * DefaultMovementSpeed * Tick * m;
//			if (moveY != 0)
//				y = static_cast<float>(moveY) * DefaultMovementSpeed * Tick * m;
//
//			move({ x, y });
//		}
//	}
//
//	float rotation = 0;
//	{
//		// Player rotation
//
//		sf::Vector2f MousePos = {0,0}; //Engine->MousePositionInWorld;
//		sf::Vector2f PlayerPos = getPosition();
//
//		float radians = static_cast<float>(atan2(
//			MousePos.y - PlayerPos.y,
//			MousePos.x - PlayerPos.x
//		));
//		rotation = radians * CONST_RADIANS_TO_DEG + 90.0f;
//		setRotation(rotation);
//	}
//
//	//if (currentWeapon)
//	{
//		//currentWeapon->Update(Tick, rotation, getPosition());
//	}
//
//	//resets default values
//	if (PickupBonuSpeedTimer > 0)
//	{
//		PickupBonuSpeedTimer -= Tick;
//
//		if (PickupBonuSpeedTimer <= 0)
//		{
//			PickupBonuSpeedTimer = 0;
//			//MovementSpeed = DefaultMovementSpeed;
//		}
//	}
}

//void Player::draw(sf::RenderTarget& Renderer, sf::RenderStates States) const
//{
////	//if (!bIsAlive) return;
////
////	Renderer.draw(*this, States);
////
////	//if (currentWeapon)
////	//{
////	//	//Renderer.draw(*currentWeapon, States);
////	//}
//
//	std::cout << "DONE3" << std::endl;
//}

//void Player::Attack()
//{
//	//if (currentWeapon)
//	//{
//	//	currentWeapon->Shoot();
//	//}
//}

//void Player::GivePlayerWeapon(Weapon *newWeapon)
//{
//	//Engine->Mutex.lock();
//	//delete currentWeapon;
//	//currentWeapon = newWeapon;
//	//Engine->Mutex.unlock();
//}

//void Player::ReloadWeapon()
//{
//	//if (currentWeapon)
//	//{
//	//	currentWeapon->Reload();
//	//}
//}
//
//void Player::RemoveWeapon()
//{
//	//if (currentWeapon)
//	//{
//	//	delete currentWeapon;
//	//	currentWeapon = nullptr;
//	//}
//}

Player::~Player()
{
	//if (currentWeapon)
	//{
	//	delete currentWeapon;
	//	currentWeapon = 0;
	//}

	MousePositionInWorld = nullptr;
}

//void Player::TakeDamage(int Damage)
//{
//	CurrentHealth -= Damage;
//
//	if (CurrentHealth <= 0)
//	{
//		CurrentHealth = 0;
//		bIsAlive = false;
//	}
//}

#include "Enemy.h"
#include "../Math/EMath.h"
#include "Player.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "ParticleSystem.h"
#include "Slime.h"

ListEnemies Enemy::StaticEnemyList;

Enemy::Enemy(Player **GamePlayer, GameEngine *Engine, sf::Vector2f SpawnPosition)
{
	this->GamePlayer = GamePlayer;
	this->Engine = Engine;
	this->setPosition(SpawnPosition);

	setOrigin(
		getLocalBounds().width * 0.5f,
		getLocalBounds().height * 0.5f
	);
	moveSpeed2 = MovementSpeed;
	CurrentHealth = 100;
}

void Enemy::Update(const float Tick)
{
//	// Jei yra followeris
//	if (GamePlayer && *GamePlayer)
//	{
//		// Vectors
//		sf::Vector2f PlayerPosition = (*GamePlayer)->GetPosition();
//		sf::Vector2f EnemyPosition = getPosition();
//		sf::Vector2f DeltaVector = PlayerPosition - EnemyPosition;
//		sf::Vector2f NormilizedVector = Math::Normalize(DeltaVector);
//
//		// Rotation
//		float Rotation = (atan2(NormilizedVector.y, NormilizedVector.x)) * CONST_RADIANS_TO_DEG + 90;
//		Rotation = Rotation < 0 ? Rotation : Rotation + 360;
//		setRotation(Rotation);
//
//		// Gaunam distance tarp prieso ir player
//		float MoveDistance = Math::GetLength(200 * Tick * NormilizedVector);
//
//		if (MoveDistance < Math::Distance(PlayerPosition, EnemyPosition))
//		{
//			// Moving
//			move(MovementSpeed * Tick * Math::Normalize(DeltaVector));
//		}
//		else
//		{
//			// Kad "nesitrankytu"
//			// Ta prasme, buna atsumas labai mazas (mazesnis nei speed'as)
//			// ir pradeda lakstyti pirmyn ir atgal bandydamas nutaikyti i vidury
//			// etc.. Norint pamatyti galit uzkomentuoti si koda ir pastebeti, kas vykta
//			// kai enemy ant playerio
//			setPosition(PlayerPosition);
//		}
//	}
//
//	if (moveSpeed2 > MovementSpeed)
//	{
//		MovementSpeed += moveSpeed2 * 0.2f * Tick;
//	}
//
}

Enemy::~Enemy()
{

}

int Enemy::Attack()
{
	return Damage;
}

void Enemy::TakeDamage(int Damage)
{
	if (!bIsAlive) return;

	new ParticleSystem(200, getPosition());

	if (CurrentHealth > 0)
		CurrentHealth -= Damage;

	SlowDown();

	if (CurrentHealth <= 0)
	{
		CurrentHealth = 0;
		bIsAlive = false;
		return;
	}
}

bool Enemy::IsAlive()
{
	return bIsAlive;
}

void Enemy::SlowDown()
{
	MovementSpeed = moveSpeed2 * 0.5f;
}

bool Enemy::InRange(sf::Vector2f vec)
{
	if (Math::Distance(getPosition(), vec) < getLocalBounds().width * 0.5f)
	{
		return true;
	}
	return false;
}

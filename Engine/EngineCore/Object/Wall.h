#pragma once
#include "Entity.h"
#include <SFML/System/Vector2.hpp>

class Wall;
class GameEngine;

using ListWalls = std::vector<Wall>;

class Wall : public Entity
{
protected:

	bool Destroy;

public:

	// Constructor
	Wall(GameEngine *Engine, sf::Vector2f EntityPosition);

	//update function
	void Update(const float Tick);

	// Destructor
	~Wall();

	// Get wall position
	sf::Vector2f GetPosition();

	// Wall list
	static ListWalls StaticWallList;
};


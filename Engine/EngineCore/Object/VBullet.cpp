#include "VBullet.h"
#include "../Math/EMath.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"

ListBullets VBullet::StaticBulletList;

VBullet::VBullet(GameEngine *Engine, sf::Vector2f Position, sf::IntRect Rect,
	float MaxTravelDistance, float TravelSpeed, float Rotation, unsigned int Damage)
{
	// TODO:
	// Perdaryti kulkos skridima i vektoriu, o ne pagal kampus (sin, cos)

	this->Damage = Damage;
	this->MaxTravelDistance = MaxTravelDistance;
	this->TravelSpeed = TravelSpeed;
	this->Rotation = Rotation;

	this->CurrentTravelledDistance = 0;
	this->Destroy = false;

	//this->Sprite.setTexture(*Engine->GameContent->GetTextures()["Projectiles"]);
	this->Sprite.setTextureRect(Rect);
	this->Sprite.setPosition(Position);
	this->Sprite.setRotation(this->Rotation);
	this->Sprite.setOrigin(2, 2);

	this->Angle = this->Rotation * CONST_DEG_TO_RADIANS;
	this->Sin = static_cast<float>(std::sin(Angle));
	this->Cos = static_cast<float>(std::cos(Angle));
}

VBullet::~VBullet()
{
}

void VBullet::Update(const float Tick)
{
	// Tikriname kelio ilgi
	if (CurrentTravelledDistance < MaxTravelDistance)
	{
		// Trackinam nueita kelia
		CurrentTravelledDistance += TravelSpeed * Tick;

		// Sprite judejimas
		Sprite.move(TravelSpeed * Sin * Tick, -TravelSpeed * Cos * Tick);

		LastPosition = Sprite.getPosition();
	}
	else
	{
		// Jei kelio ilgis buvo pasiektas
		Destroy = true;
	}
}

void VBullet::draw(sf::RenderTarget& Renderer, sf::RenderStates States) const
{
	Renderer.draw(this->Sprite, States);
}

bool VBullet::DestroyState()
{
	// Grazina objekto busena (ar ji naikinti)
	return this->Destroy;
}

sf::Vector2f VBullet::GetPosition()
{
	return Sprite.getPosition();
}

unsigned int VBullet::GetDamage()
{
	return Damage;
}

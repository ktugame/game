#pragma once

#include "Weapon.h"

// WPistol1 ginklas
class WPistol1 : public Weapon
{
public:

	// Consturctor
	// Engine pointer
	// Bullet list pointer
	// Player pointer
	WPistol1(GameEngine *Engine);

	// Destructor
	~WPistol1();

};

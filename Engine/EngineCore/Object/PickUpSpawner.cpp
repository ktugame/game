#include "PickUpSpawner.h"
#include "PHealth.h"
#include "PSpeed.h"
#include "PWeapon.h"
#include "../Math/EMath.h"

PickUpSpawner::PickUpSpawner(GameEngine *Engine, Player **GamePlayer)
	//: Entity(Engine, { 0,0 })
{
	this->Engine = Engine;
	this->GamePlayer = GamePlayer;
	this->SpawnWidth = 2000;
	this->SpawnHeigth = 2000;

	this->MinTime = 1.0f;
	this->MaxTime = 3.0f;
	this->MinPickupLifeTime = 15.0f;
	this->MaxPickupLifeTime = 35.0f;
	this->CurrTimer = 0.0f;
	this->SpawningTimer = Math::Random(MinTime, MaxTime);
	this->SpawnWidth = SpawnWidth;
	this->SpawnHeigth = SpawnHeigth;
}

PickUpSpawner::~PickUpSpawner()
{
	this->GamePlayer = nullptr;
}

void PickUpSpawner::Update(const float Tick)
{
	{
		for (; CurrTimer > SpawningTimer; CurrTimer -= SpawningTimer)
		{
			sf::Vector2f spawnerPosition = this->getPosition();

			float lifeTime = Math::Random(MinPickupLifeTime, MaxPickupLifeTime); // generate pickup life time

			sf::Vector2f pickUpPosition = 
				sf::Vector2f(Math::Random(-SpawnWidth + spawnerPosition.x, SpawnWidth + spawnerPosition.x),
				Math::Random(-SpawnHeigth + spawnerPosition.y, SpawnHeigth + spawnerPosition.x));

			int pickUpOption = Math::Random(0, 2);

			switch (pickUpOption)
			{
			case 0:
				PickUp::StaticPickupList.push_back(new PHealth(Engine, GamePlayer, pickUpPosition, lifeTime));
				break;
			case 1:
				PickUp::StaticPickupList.push_back(new PWeapon(Engine, GamePlayer, pickUpPosition, lifeTime));
				break;
			case 2:
				PickUp::StaticPickupList.push_back(new PSpeed(Engine, GamePlayer, pickUpPosition, lifeTime));
				break;

			default:
				PickUp::StaticPickupList.push_back(new PickUp(Engine, GamePlayer, pickUpPosition, lifeTime));
				break;
			}

			SpawningTimer = Math::Random(MinTime, MaxTime); // when pickup spawns generate new spawning time
		}
		CurrTimer += Tick;
	}
}

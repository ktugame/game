#include "PWeapon.h"
#include "../Math/EMath.h"
#include "WDesertEagle.h"
#include "WMossberg590.h"
#include "WPistol1.h"
#include "../Engine/ContentLoader.h"
#include "../Engine/GameEngine.h"

PWeapon::PWeapon(GameEngine *Engine, Player **GamePlayer,
	sf::Vector2f EntityPosition, float LiveTimer)
	: PickUp(Engine, GamePlayer, EntityPosition, LiveTimer)
{
	this->Type = pWeapon;
	this->IsTaken = false;
	this->setTextureRect({ 64 * 2, 64 * 0, 64 * 1, 64 * 1 });
	//this->GenerateWeapon();
}

void PWeapon::GenerateWeapon()
{
	switch (Math::Random(0, 2))
	{
	case 1:
	{
		weapon = new WMossberg590(Engine);
	}
	break;

	case 2:
	{
		weapon = new WPistol1(Engine);
	}
	break;

	default:
	{
		weapon = new WDesertEagle(Engine);
	}break;

	}

}

PWeapon::~PWeapon()
{
	if (IsTaken) return;

	delete weapon;
	weapon = nullptr;
}

Weapon *PWeapon::NewWeapon()
{
	if (IsTaken) nullptr;

	//if (!weapon) return;

	IsTaken = true;

	//return weapon;
	return new WMossberg590(Engine);
}

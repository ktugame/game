#pragma once

#include <SFML/Graphics/Sprite.hpp>

// Entity class
// This class is for the world map objects
// You can have dinamic or static objects (moveable and immmovable)
// Static - buildings, trees, pickups, walls...
// Dinamic - player, enemies, bugs...
class Entity : public sf::Sprite
{
public:

	virtual ~Entity();

	virtual void Update(const float Tick) = 0;

};

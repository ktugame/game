#include "Bug.h"
#include "Enemy.h"
#include "../Engine/ContentLoader.h"
#include "../Engine/GameEngine.h"
#include "../Math/EMath.h"

Bug::Bug(Player **GamePlayer, GameEngine *Engine, sf::Vector2f SpawnPosition)
	: Enemy(GamePlayer, Engine, SpawnPosition)
{
	this->setPosition(SpawnPosition);
	//this->setTexture(*Engine->GameContent->GetTextures()["EnemyBug"]);
	this->setTextureRect(sf::IntRect(0, 0, 64, 64));
	this->setOrigin(
		getLocalBounds().width / 2,
		getLocalBounds().height / 2
	);

	sf::Vector2f scale(0, 0);
	scale.x = scale.y = static_cast<float>(Math::Random(7.0f, 17.5f)) / 10.0f;
	this->setScale(scale);

	CurrentHealth = static_cast<int>(15 + 30 * scale.y);

	Damage = static_cast<int>(10 + 15 * scale.y);

	MovementSpeed = 200.0f + 100.0f / scale.y;
	
	//bIsSlime = false;
}

Bug::~Bug()
{
}

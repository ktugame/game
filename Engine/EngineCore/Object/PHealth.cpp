#include "PHealth.h"
#include "../Math/EMath.h"

PHealth::PHealth(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, float LiveTimer)
	: PickUp(Engine, GamePlayer, EntityPosition, LiveTimer)
{
	this->Type = pHealth;
	this->setTextureRect({ 64 * 1, 64 * 0, 64 * 1, 64 * 1 });
}

PHealth::~PHealth()
{

}

unsigned int PHealth::HealthAmount() const
{
	return static_cast<unsigned int>(Math::Random(3,5) * 10);
}

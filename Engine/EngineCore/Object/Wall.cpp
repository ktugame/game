#include "Wall.h"
#include "Player.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"

ListWalls Wall::StaticWallList;

Wall::Wall(GameEngine *Engine, sf::Vector2f EntityPosition)
	//: Entity(Engine, EntityPosition)
{
	//this->setTexture(*Engine->GameContent->GetTextures()["WallTile"]);
	this->setOrigin({ 32, 32 });
	this->Destroy = false;
}

Wall::~Wall()
{

}

void Wall::Update(const float Tick)
{

}

sf::Vector2f Wall::GetPosition()
{
	return this->getPosition();
}

#pragma once

#include "Actor.h"

class Player;
class GameEngine;
class Enemy;

using ListEnemies = std::vector<Enemy*>;

// Enemy klase skirta priesam, kurie bandys pulti player'i ir ji nuzudyti
class Enemy : public Actor
{
protected:

#ifndef TESTING

	// Player pointer
	Player **GamePlayer;

	// Engine pointer
	GameEngine *Engine;

#endif

	// Bonus moving speed for restoring starting speed
	float moveSpeed2;


public:

#ifndef TESTING

	// Constructor
	// Player pointer to the pointer
	// Spawning position
	Enemy(Player **GamePlayer, GameEngine *Engine, sf::Vector2f SpawnPosition);

	// Update actor
	void Update(const float Tick);

#else

	Enemy(sf::Vector2f SpawnPosition, int Damage);

#endif

	// Destructor
	~Enemy();

	// Enemy attacks
	int Attack();

	// Actor takes damage. dmg - amount of damage
	virtual void TakeDamage(int Damage);

	// Gets object state
	bool IsAlive();

	//Check if object is in range with enemy
	bool InRange(sf::Vector2f vec);

	// List of enemies
	static ListEnemies StaticEnemyList;

	// Enemy damage
	int Damage;

protected:

	// Change speed
	void SlowDown();

};

#include "Weapon.h"
#include "../Math/EMath.h"
#ifndef TESTING
#include "VBullet.h"
#include "../GUI/UIManager.h"
#include "../GUI/UIText.h"
#include "../GUI/Widget.h"
#include "../Engine/GameEngine.h"
#include "Player.h"
#include "../Engine/ContentLoader.h"
#include "../Engine/SoundManager.h"

#define toStr(message) std::to_string(message)
#endif

#ifndef TESTING

Weapon::Weapon(GameEngine *Engine, unsigned int AmmoPerClip, unsigned int Damage,
	float FireSpeed, float ReloadSpeed, float Range, float Accuracy,
	bool bAutomatic)
{
	this->Engine = Engine;

	this->AmmoCurrent = AmmoPerClip;
	this->AmmoPerClip = AmmoPerClip;

	this->Damage = Damage;

	this->FireSpeed = FireSpeed;
	this->ReloadSpeed = ReloadSpeed;

	this->Range = Range;

	this->Accuracy = Accuracy;

	this->bAutomatic = bAutomatic;

	this->bShooted = false;
	this->bReloading = false;

	this->FireTimer = 0.0f;
	this->ReloadTimer = 0.0f;

	///this->setTexture(*Engine->GameContent->GetTextures()["WPistol1"]);
	this->setTextureRect(sf::IntRect({ 0,0,128,128 }));
	this->setOrigin({ 64, 64});

	//this->WeaponWidget = new Widget();
	//
	//sf::View GameView = Engine->GameWindow->getView();
	//sf::Vector2f TextPosition = -GameView.getSize() / 2.0f;
	//TextPosition.y += GameView.getSize().y / 2;
	//TextPosition.y += 105;
	//
	//this->uiAmmoText = new UIText(Engine);
	//this->uiAmmoText->SetText(toStr(AmmoCurrent) + "/" + toStr(AmmoPerClip));
	//this->uiAmmoText->SetPosition(TextPosition);
	//
	//TextPosition.y += 45;
	//
	//this->uiReloadTimer = new UIText(Engine);
	//this->uiReloadTimer->SetText("Reloading: 0%");
	//this->uiReloadTimer->SetPosition(TextPosition);
	//
	//TextPosition.y += 45;
	//
	//this->uiFireTimer = new UIText(Engine);
	//this->uiFireTimer->SetText("Firing: 0%");
	//this->uiFireTimer->SetPosition(TextPosition);
	//
	//this->WeaponWidget->AddUIElement(this->uiAmmoText);
	//this->WeaponWidget->AddUIElement(this->uiReloadTimer);
	//this->WeaponWidget->AddUIElement(this->uiFireTimer);
	//
	//this->uiAmmoText->SetColor(sf::Color(100, 100, 100, 200));
	//this->uiReloadTimer->SetColor(sf::Color(100, 100, 100, 200));
	//this->uiFireTimer->SetColor(sf::Color(100, 100, 100, 200));
	//
	//Engine->GameUIManager->AddWidget(WeaponWidget);
}

sf::Sprite *Weapon::GetSprite()
{
	return this;
}

void Weapon::Update(const float Tick, float Rotation, sf::Vector2f Position)
{
	setPosition(Position);
	setRotation(Rotation);

	if (bShooted)
	{
		if (FireTimer < FireSpeed)
		{
			FireTimer += Tick;
		}
		else
		{
			bShooted = false;
			FireTimer = 0;
		}
	}

	if (bReloading)
	{
		if (ReloadTimer < ReloadSpeed)
		{
			ReloadTimer += Tick;
		}
		else
		{
			bReloading = false;
			ReloadTimer = 0;

			this->CalculateAmmo();
		}
	}

	// Updating text

	//uiAmmoText->SetText(toStr(AmmoCurrent) + "/" + toStr(AmmoPerClip));
	//uiFireTimer->SetText("Firing: " + toStr(static_cast<int>((FireTimer / FireSpeed) * 100)) + "%");
	//uiReloadTimer->SetText("Reloading: " + toStr(static_cast<int>((ReloadTimer / ReloadSpeed) * 100)) + "%");

}

#else

Weapon::Weapon(unsigned int AmmoPerClip, unsigned int Damage,
	float FireSpeed, float ReloadSpeed, float Range, float Accuracy,
	bool bAutomatic)
{
	this->AmmoCurrent = AmmoPerClip;
	this->AmmoPerClip = AmmoPerClip;

	this->Damage = Damage;

	this->FireSpeed = FireSpeed;
	this->ReloadSpeed = ReloadSpeed;

	this->Range = Range;

	this->Accuracy = Accuracy;

	this->bAutomatic = bAutomatic;

	this->bShooted = false;
	this->bReloading = false;

	this->FireTimer = 0.0f;
	this->ReloadTimer = 0.0f;
}
#endif

Weapon::~Weapon()
{
#ifndef TESTING
	//WeaponWidget->DestoyWidget();
#endif
}

bool Weapon::CheckWeaponStatus()
{
	if (!bShooted && !bReloading)
	{
		if (AmmoCurrent > 0)
		{
			return true;
		}
		else
		{
			this->Reload();
		}
	}
	return false;
}


void Weapon::Shoot()
{
	if (!this->CheckWeaponStatus())
		return;

	sf::Vector2f Position = getPosition();
	float Rotation = getRotation() + Math::Random(-25.5f, 25.5f) * (1.0f - Accuracy);

	VBullet::StaticBulletList.push_back(
		VBullet(Engine, Position, sf::IntRect({ 2, 0 }, { 16, 32 }),
			this->Range, 1700.0f, Rotation, Damage));

	//Engine->GameSoundManager->Play("PistolShot1");

	bShooted = true;

	AmmoCurrent--;
}

void Weapon::Reload()
{
#ifndef TESTING

	if (!bReloading)
	{
		if (AmmoCurrent < AmmoPerClip)
		{
			bReloading = true;
			ReloadTimer = 0;

			//Engine->GameSoundManager->Play("ShotgunPump1");
		}
	}

#else

	this->bReloading = true;
	this->ReloadTimer = 0;
	this->CalculateAmmo();

#endif

}

int Weapon::GetCurrentAmmo() const
{
	// Grazina kiek ammo yra dabar
	return this->AmmoCurrent;
}

int Weapon::GetAmmoPerClip() const
{
	// Grazina kiek ammo per clip
	return this->AmmoPerClip;
}

bool Weapon::IsAutomatic() const
{
	// Grazina ginklo tipa (automatinis / neautomatinis)
	return this->bAutomatic;
}

void Weapon::CalculateAmmo()
{
	AmmoCurrent = AmmoPerClip;
}

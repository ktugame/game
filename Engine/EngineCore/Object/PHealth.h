#pragma once

#include "PickUp.h"

class PHealth : public PickUp
{
private:

public:

#ifndef TESTING

	// Constructor
	// Engine pointer
	// Player pointer
	// Entity position
	// Living time
	PHealth(GameEngine *Engine, Player **GamePlayer, sf::Vector2f EntityPosition, float LiveTimer);

#else
	// Constrcutor for testing
	PHealth(sf::Vector2f EntityPosition, float LiveTimer);

#endif

	// Destructor
	~PHealth();

	// HP kiekis++
	// Returns 30, 40, 50 HP
	unsigned int HealthAmount() const;

};

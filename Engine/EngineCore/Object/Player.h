#pragma once

//#include "Actor.h"

#include <SFML/Graphics.hpp>

//class PickUp;
//class Weapon;
//class GameEngine;

// Class for the player
class Player : public sf::Sprite
//: public Actor
{
private:

	// Player's current weapon
	//Weapon *currentWeapon;

	// Engine pointer
	//GameEngine *Engine;

	// Mouse position in the world
	sf::Vector2f *MousePositionInWorld;

	// Player max health
	const int MaxHealth = 200;

	// Movement speed
	const float DefaultMovementSpeed = 250;

	// Power up duration timer
	float PickupBonuSpeedTimer;

public:

	// Constructor
	// Engine pointer
	// Spawn position
	Player(sf::Vector2f SpawnPosition);

	// Destructor
	~Player();

	// Player takes damage. Returns if player died or not (!bAlive);
	//void TakeDamage(int Damage);

	// Reloads weapon
	//void ReloadWeapon();

	// Attack
	//void Attack();

	// Updates player
	void Update(const float Tick);

	// Gives player new weapon
	//void GivePlayerWeapon(Weapon *newWeapon);

	// Add pickup and some bonuses
	//void AddPickupStatus(PickUp *p);

private:

	using DIR = char;

	// Draw method (SFML)
	//void draw(sf::RenderTarget& Renderer, sf::RenderStates States) const;

	// Remove weapon
	//void RemoveWeapon();

};

#ifndef CONSOLE_H
#define CONSOLE_H
#pragma once

#include <SFML/Graphics.hpp>
#include <functional>
#include <vector>
#include <set>
#include <map>
#include <string.h>

using std::map;
using std::string;
using FunctionPointer = std::function<void(void)>*;
using ListCommand = map<string, FunctionPointer>;
using ListText = std::vector<string>;

// Converting any number or object(?) to string
#define toStr(number)		std::to_string(number)
// Converting any number or object(?) to string
#define toString(number)	std::to_string(number)
// Converting string to long
#define toLong(string_)		std::stol(string_)
// Converting string to float
#define toFloat(string_)	std::stof(string_)
// Converting string to double
#define toDouble(string_)	std::stod(string_)

// Game console for output
class Console
{
private:

	// Game window
	sf::RenderWindow *GameWindow;

	// UI View
	sf::View *UIView;


	// All messages...?
	// Optimize later
	sf::Text Text;

	// Is console hidden?
	bool Hidden;

	// If text changed
	bool Changed;

	// Grey transpared background
	sf::RectangleShape BackgroundColor;

	// Console text position
	sf::Vector2f ConsoleTextPosition;

	// Console text offset
	sf::Vector2f ConsoleOffset;

	// Console command list
	ListCommand Commands;

	// Test list
	ListText list;

public:

	// Console constructor
	Console();

	// Desturctor
	~Console();


	// Give ready status
	// This function calls other functions:
	// * ChangeWindow
	// * ChangeFont
	// * ChangeUIView
	void Ready(sf::RenderWindow *GameWindow, sf::Font *Font, sf::View *UIView);

	// Add Font
	inline void AddFont(sf::Font *Font) { this->Text.setFont(*Font); }
	// Add or change Font
	// This function calls other function:
	// * AddFont
	inline void ChangeFont(sf::Font *Font) { AddFont(Font); }

	// Add GameWindow
	inline void AddGameWindow(sf::RenderWindow *GameWindow) { this->GameWindow = GameWindow; }
	// Add or change GameWindow
	// This function calls other function:
	// * AddGameWindow
	inline void ChangeGameWindow(sf::RenderWindow *GameWindow) { AddGameWindow(GameWindow); }

	// Add UIView
	inline void AddUIView(sf::View *UIView) { this->UIView = UIView; }
	// Add or change UIView
	// This function calls other function:
	// * AddUIView
	inline void ChangeUIView(sf::View *UIView) { AddUIView(UIView); }


	// Update console
	void Update();

	// Draw console
	void Draw();

	// Execute command
	void ExecuteCommand(string command);

	// Add console line
	// Gal tai perdaryti i template, kad priimtu iskart bet kokia reiksme.. ?
	// Bet po kol kas bus taip, atrodo paprasciau, kad ir daug metodu
	void
		AddLine(string line, string str = "Engine: "),
		AddLine(float val, string str = "float: "),
		AddLine(double val, string str = "double: "),
		AddLine(int val, string str = "int: "),
		AddLine(unsigned int val, string str = "uint: "),
		AddLine(sf::FloatRect bounds, string str = "bounds: "),
		AddLine(sf::Vector2f vec, string str = "vector x y:");

	// Add command
	void AddCommand(string command, FunctionPointer function);

	// Show console
	void Show();

	// Hide console
	void Hide();

	// Turn on/off console
	void Toggle();

	// Get console status (for disabling all input from player in the game)
	bool IsHidden();

private:

	template <class T>
	void NewLine(T t, string str)
	{
		list.insert(list.begin(),
			(str + "\t"
				+ toStr(t)
				+ "\n"
				));

		this->Changed = true;
	}

};

#endif // !CONSOLE_H

#include "Console.h"

Console::Console()
{
	this->GameWindow = nullptr;
	this->UIView = nullptr;

	this->ConsoleTextPosition = sf::Vector2f(0, 0);
	this->ConsoleOffset = ConsoleTextPosition;

	this->Text.setString("");
	this->Text.setCharacterSize(15);
	this->Text.setPosition(ConsoleTextPosition);

	this->BackgroundColor.setFillColor(sf::Color(33, 33, 33, 220));
	this->BackgroundColor.setPosition(ConsoleTextPosition);

	this->Changed = true;
	this->Hidden = true;

}

Console::~Console()
{
	this->GameWindow = nullptr;
	this->UIView = nullptr;
}

void Console::Ready(sf::RenderWindow *GameWindow, sf::Font *Font, sf::View *UIView)
{
	ChangeFont(Font);
	ChangeUIView(UIView);
	ChangeGameWindow(GameWindow);
}

void Console::Update()
{
	if (!(GameWindow && UIView && Text.getFont()))
	{
		throw "GameWindow or UIView or Text.getFont() is null!";
	}

	if (UIView)
	{
		sf::Vector2f ViewSize = UIView->getSize();

		this->ConsoleOffset = ViewSize / -2.0f;
		this->ConsoleOffset.y = -this->ConsoleOffset.y - 50;
		this->ConsoleOffset.x += 20;

		this->BackgroundColor.setSize(ViewSize);
		this->BackgroundColor.setOrigin(ViewSize / 2.0f);
	}

	if (Changed)
	{
		ConsoleTextPosition.y = Text.getLocalBounds().height;

		Text.setPosition(-ConsoleTextPosition + ConsoleOffset);

		Changed = false;
	}
}

void Console::Draw()
{
	if (!(GameWindow && UIView && Text.getFont()))
	{
		throw "GameWindow or UIView or Text.getFont() is null!";
	}

	if (!Hidden)
	{
		if (GameWindow && UIView)
		{
			GameWindow->setView(*UIView);
			GameWindow->draw(BackgroundColor);

			unsigned long Size = list.size();

			if (Size > 50)
			{
				list.erase(list.begin() + 50, list.end());
			}

			Size = list.size();

			for (unsigned long i = 0; i < (Size > 50 ? 50 : Size); i++)
			{
				Text.setString(list[i]);
				Text.setPosition(sf::Vector2f(ConsoleOffset.x, i * -20.0f + ConsoleOffset.y));
				GameWindow->draw(Text);
			}

		}
	}
}

void Console::ExecuteCommand(string command)
{
	if (Commands[command])
	{
		(*Commands[command])();
	}
}

void Console::AddLine(string line, string str)
{
	list.insert(list.begin(),
		(str + "\t"
			+ line
			+ "\n"
			));

	this->Changed = true;
}

void Console::AddLine(float val, string str)
{
	NewLine(val, str);
}

void Console::AddLine(double val, string str)
{
	NewLine(val, str);
}

void Console::AddLine(int val, string str)
{
	NewLine(val, str);
}

void Console::AddLine(unsigned int val, string str)
{
	NewLine(val, str);
}

void Console::AddLine(sf::FloatRect bounds, string str)
{
	list.insert(list.begin(),
		(str + "\t"
			+ "l: " + toStr(bounds.left)
			+ "t: " + toStr(bounds.top)
			+ "w: " + toStr(bounds.width)
			+ "h: " + toStr(bounds.height)
			+ "\n"
			));

	this->Changed = true;
}

void Console::AddLine(sf::Vector2f vec, string str)
{
	list.insert(list.begin(),
		(str + "\t"
			+ "x: " + toStr(vec.x)
			+ "y: " + toStr(vec.y)
			+ "\n"
			));

	this->Changed = true;
}

void Console::Show()
{
	Hidden = false;
}

void Console::Hide()
{
	Hidden = true;
}

void Console::Toggle()
{
	Hidden = !Hidden;
}

void Console::AddCommand(string command, FunctionPointer function)
{
	Commands[command] = function;
}

bool Console::IsHidden()
{
	return Hidden;
}

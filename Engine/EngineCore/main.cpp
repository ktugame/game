#include "Engine/GameEngine.h"

#ifdef OS_WINDOWS
int LaunchEngine(HINSTANCE hInstance, LPSTR lpCmdLine, int nCmdShow)
#elif OS_LINUX
int LaunchEngine(char **argv)
#endif
{
#ifdef OS_WINDOWS
	GameEngine *Engine = new GameEngine(lpCmdLine);
#elif OS_LINUX
	GameEngine *Engine = new GameEngine(argv);
#endif

	if (Engine->Ready())
	{
		Engine->Run();

		delete Engine;
		Engine = nullptr;
		return 0;
	}
	else
	{
		delete Engine;
		Engine = nullptr;
		return 1;
	}
}

#ifdef OS_WINDOWS
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	return LaunchEngine(hInstance, lpCmdLine, nCmdShow);
}
#elif OS_LINUX
int main(int argc, char **argv) { return LaunchEngine(argv); }
#endif

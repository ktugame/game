#pragma once

#include <string>
#if defined(DEBUG) && defined(OS_WINDOWS)
#include <Windows.h>

#define MESSAGE(message) OutputDebugString((std::string("DEBUG: ") + message + "\n").c_str())

#elif defined(OS_WINDOWS) && !defined(DEBUG)
#include <Windows.h>

//#define MESSAGE(message) MessageBox(0, message, "Engine Error", MB_OK | MB_ICONERROR)
#define MESSAGE(message) OutputDebugString((std::string("DEBUG: ") + message + "\n").c_str())

#endif

#ifdef OS_LINUX

#include <iostream>
using std::cout;
using std::endl;

#endif

#if defined(DEBUG) && defined(OS_LINUX)

#define MESSAGE(message) cout << "DEBUG: " << message << endl

#elif defined(OS_LINUX)

#define MESSAGE(message) cout << "Engine Error: " << message << endl

#endif

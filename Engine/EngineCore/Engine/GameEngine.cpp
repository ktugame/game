#include "GameEngine.h"
#include "Settings.h"
#include "FPSCounter.h"
#include "../Object/Player.h"

#ifdef OS_WINDOWS
GameEngine::GameEngine(LPSTR lpCmdLine)
#elif OS_LINUX
GameEngine::GameEngine(char **argv)
#endif
{
	engineState = INITIALIZING;

	srand(static_cast<uint>(time(0)));

	engineSettings = unique_ptr<Settings>(new Settings(argv[0]));

	auto DesktopMode = sf::VideoMode::getDesktopMode();
	desktopSize = sf::Vector2i(DesktopMode.width, DesktopMode.height);

	fpsCounter = unique_ptr<FPSCounter>(new FPSCounter());

	keyActionMap[sf::Keyboard::Escape] = KeyActions::Exit;
	keyActionMap[sf::Keyboard::Tilde] = KeyActions::Exit;
}

GameEngine::~GameEngine()
{
	// TODO: collect and display errors
}

void GameEngine::Run()
{
	if (engineState == SHUTTINGDOWN)
	{
		return;
	}

	engineState = RUNNING;

	bIsWindowFocused = true;

	auto WindowStyle = engineSettings->isFullScreen ?
		sf::Style::Fullscreen : sf::Style::Default;

	auto WindowSize = engineSettings->isFullScreen ?
		desktopSize : engineSettings->windowSize;

	auto WindowAndDesktopDiffHalf = (desktopSize - 	engineSettings->windowSize) / 2;

	auto WindowPosition = sf::Vector2i(
		engineSettings->windowPosition.x < 0 ?
			WindowAndDesktopDiffHalf.x : engineSettings->windowPosition.x,
		engineSettings->windowPosition.y < 0 ?
		WindowAndDesktopDiffHalf.y : engineSettings->windowPosition.y
	);

	engineWindow = unique_ptr<sf::RenderWindow>(
		new sf::RenderWindow(
			sf::VideoMode(static_cast<uint>(WindowSize.x), static_cast<uint>(WindowSize.y)),
			this->windowTitle,
			WindowStyle,
			*engineSettings
		)
	);

	engineWindow->setPosition(WindowPosition);

	engineWindow->setFramerateLimit(engineSettings->framesPerSecond < 5 ?
		 0 : static_cast<uint>(engineSettings->framesPerSecond)
	);

#ifdef DEBUG
	engineSettings->PrintVariables();
#endif

	Player *player = new Player({0, 0});

	while (engineWindow->isOpen())
	{
		// Frame calculations
		fpsCounter->CalcDiff();

		// Poll events
		PollEvents();

		// TODO: for TESTING only. Delete/export this later
		{
			for (auto &item : keySet)
			{
				if (keyActionMap[item] == KeyActions::Exit)
				{
					engineState = EngineState::SHUTTINGDOWN;
				}
			}
		}

		// Update
		if (bIsWindowFocused)
		{
			player->Update(fpsCounter->GetTick());
		}

		// Draw
		engineWindow->clear(sf::Color::Black);
		engineWindow->draw(*player);
		engineWindow->display();

		// Shut down
		if (engineState == EngineState::SHUTTINGDOWN)
		{
			engineWindow->close();
			break;
		}
	}

	delete player;
}

inline void GameEngine::WindowLostFocus()
{
	this->bIsWindowFocused = false;
	this->keySet.clear();
	this->mouseSet.clear();
}

inline void GameEngine::WindowGainedFocus()
{
	bIsWindowFocused = true;
}

void GameEngine::PollEvents()
{
	sf::Event Event;

	while (engineWindow->pollEvent(Event))
	{
		switch (Event.type)
		{

		case sf::Event::KeyPressed:
		{
			KeyDown(Event.key.code);
		}
		break;

		case sf::Event::KeyReleased:
		{
			KeyUp(Event.key.code);
		}
		break;

		case sf::Event::MouseMoved:
		{
			MousePositionInWorld =
					engineWindow->mapPixelToCoords(
					MousePositionOnScreen = sf::Mouse::getPosition(*engineWindow)
				);
		}
		break;

		case sf::Event::MouseButtonPressed:
		{
			MouseDown(Event.mouseButton.button);
		}
		break;

		case sf::Event::MouseButtonReleased:
		{
			MouseUp(Event.mouseButton.button);
		}
		break;

		case sf::Event::MouseEntered:
		{
			mouseWindowStatus = MouseWindowStatus::InsideWindow;
		}
		break;

		case sf::Event::MouseLeft:
		{
			mouseWindowStatus = MouseWindowStatus::OutsideWindow;
		}
		break;

		case sf::Event::GainedFocus:
		{
			WindowGainedFocus();
		}
		break;

		case sf::Event::LostFocus:
		{
			WindowLostFocus();
		}
		break;

		case sf::Event::Closed:
		{
			engineState = SHUTTINGDOWN;
		}
		break;

		case sf::Event::Resized:
		{
			Resize(engineWindow);
		}
		break;

		default:
			break;
		}

	}
}

void GameEngine::Resize(const unique_ptr<sf::RenderWindow> &window)
{
	auto view = window->getView();
	auto viewPort = window->getViewport(view);
	float width = static_cast<float>(viewPort.width);
	float height = static_cast<float>(viewPort.height);
	window->setView(sf::View({ 0, 0 }, { width, height }));
}

bool GameEngine::IsKeyDown(sf::Keyboard::Key Key)
{
	// TODO: this statement can be simplified
	if (keySet.find(Key) != keySet.end())
		return true;

	return false;
}

inline void GameEngine::KeyDown(sf::Keyboard::Key Key)
{
	keySet.insert(Key);
}

inline void GameEngine::KeyUp(sf::Keyboard::Key Key)
{
	KEY_SET::iterator iterator = keySet.find(Key);

	if (iterator != keySet.end())
		keySet.erase(iterator);
}

bool GameEngine::IsMouseButtonDown(sf::Mouse::Button Button)
{
	// TODO: this statement can be simplified
	if (mouseSet.find(Button) != mouseSet.end())
		return true;

	return false;
}

inline void GameEngine::MouseDown(sf::Mouse::Button Button)
{
	mouseSet.insert(Button);
}

inline void GameEngine::MouseUp(sf::Mouse::Button Button)
{
	MOUSE_SET::iterator iterator = mouseSet.find(Button);

	if (iterator != mouseSet.end())
		mouseSet.erase(iterator);
}

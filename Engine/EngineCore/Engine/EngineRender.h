#ifdef OS_WINDOWS

// Created by Tomas Zigmantavicius on 02 / 12
#pragma once
// Included in EngineWin.h
//#include "SDL_opengl.h"
#include <SFML/OpenGL.hpp>
//#include "Texture.h"
//#include "EngineWin.h"

class EngineRender
{
	HWND* hWnd;
	HDC hDC;
	HGLRC hRC;
	//EngineWin* engine;
	// Functions
public:
	EngineRender(HWND*);
	void RenderObject();
	// Setups OpenGL for our window
	void SetOpenGL();
	// Disables OpenGL
	void DisableOpenGL();
	// Renders list of textures 
	void RenderLoop();
};

#endif // OS_WINDOWS

#pragma once

#include <SFML/Window/Context.hpp>
#include <SFML/Window/ContextSettings.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <fstream>

#define toCharArr(value) value.c_str()

using std::string;

using uint = unsigned int;

// Class for game settings
class Settings : public sf::ContextSettings
{
private:

	string filePath;

public:

	bool isFullScreen;
	bool isOnVerticalSync;

	sf::Vector2i windowSize;
	sf::Vector2i windowPosition;

	int framesPerSecond;

	bool showFps;

public:

#ifdef OS_WINDOWS
	Settings();
#elif OS_LINUX
	Settings(char *arg);
#endif

	~Settings();

	void WriteToFile();
	void LoadFromFile();

	// Reset all
	void ResetSettings();

#ifdef DEBUG
public:
	void PrintVariables();
#endif

};

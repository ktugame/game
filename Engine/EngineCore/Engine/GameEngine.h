#pragma once

#include <SFML/Graphics.hpp>
#include <set>

#ifdef OS_WINDOWS
#include "resource.h"
#include <windows.h>
#endif

class Settings;
struct FPSCounter;

enum KeyActions
{
	None,
	Exit,
};

using KEY_SET = std::set<sf::Keyboard::Key>;
using MOUSE_SET = std::set<sf::Mouse::Button>;

using KEY_ACTION_MAP = std::map<sf::Keyboard::Key, KeyActions>;

using uint = unsigned int;
using std::unique_ptr;

// Game engine class
class GameEngine
{
public:

	enum EngineState
	{
		INITIALIZING,
		RUNNING,
		SHUTTINGDOWN,
	};

	enum MouseWindowStatus
	{
		InsideWindow,
		OutsideWindow,
	};

public:

#ifdef OS_WINDOWS
	GameEngine(LPSTR lpCmdLine);
#elif OS_LINUX
	GameEngine(char **argv);
#endif

	~GameEngine();

private:

	// Engine settings
	unique_ptr<Settings> engineSettings;

	// Engine window
	unique_ptr<sf::RenderWindow> engineWindow;

	// FPS counter
	unique_ptr<FPSCounter> fpsCounter;

	// Holding vector of the desktopo size
	sf::Vector2i desktopSize;

	// Window status
	bool bIsWindowFocused;

	// Engine state
	EngineState engineState;

	// Pressed keyboard keys
	KEY_SET keySet;

	// Pressed mouse keys
	MOUSE_SET mouseSet;

	// Key action map
	KEY_ACTION_MAP keyActionMap;

	// Capture mouse status
	MouseWindowStatus mouseWindowStatus;

public:

	// Window status
	bool IsWindowFocused() const { return bIsWindowFocused; }

	// Resize window
	void Resize(const unique_ptr<sf::RenderWindow> &window);

	// Checks if game/engine status
	bool Ready() { return true; }

	// Run game
	void Run();

	// Poll window events
	// For eg.: keyboard clicks, mouse clicks
	void PollEvents();

	// Target resolution
	// TargetWindowWidth = 1920
	// TargetWindowHeight = 1080
	const sf::Vector2i TargetWindowResolution = { 1920, 1080 };

	// Mouse position on the screen
	sf::Vector2i MousePositionOnScreen;

	// Mouse position in the world
	sf::Vector2f MousePositionInWorld;

	// Check if the key is down
	bool IsKeyDown(sf::Keyboard::Key Key);

	// Check if the mouse button down
	bool IsMouseButtonDown(sf::Mouse::Button Button);


private:

	// Keyboard key down
	inline void KeyDown(sf::Keyboard::Key Key);

	// Keyboard key up
	inline void KeyUp(sf::Keyboard::Key Key);

	// Mouse button down
	inline void MouseDown(sf::Mouse::Button Button);

	// Mouse button up
	inline void MouseUp(sf::Mouse::Button Button);

	// Window lost focus
	inline void WindowLostFocus();

	// Window gained focus
	inline void WindowGainedFocus();

private:

#ifdef OS_WINDOWS
#ifdef OS_64
#ifdef DEBUG
	const char *windowTitle = "SALAYC - 64bit [Windows] - Debug";
#else
	const char *windowTitle = "SALAYC - 64bit [Windows]";
#endif
#elif OS_32
#ifdef DEBUG
	const char *windowTitle = "SALAYC - 32bit [Windows] - Debug";
#else
	const char *windowTitle = "SALAYC - 32bit [Windows]";
#endif
#endif
#elif OS_LINUX
#ifdef DEBUG
	const char *windowTitle = "SALAYC - 64bit [Linux] - Debug";
#else
	const char *windowTitle = "SALAYC - 64bit [Linux]";
#endif
#endif

};

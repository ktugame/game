#pragma once
#include <SFML/Audio.hpp>
#include <vector>
#include <string>

using std::string;

class ContentLoader;

// Mini sound manager
class SoundManager
{
private:

	// Sound status
	struct SoundStatus
	{
		// Sound
		sf::Sound Sound;

		// Statu
		bool StartedToPlay = false;

	};

	using SoundStack = std::vector<SoundStatus*>;

	// Sound list
	SoundStack Sounds;

	// Game content
	ContentLoader *GameContent;

public:

	// Constructor
	SoundManager(ContentLoader *GameContent);

	// Destructor
	~SoundManager();

	// Play sound by name
	void Play(string SoundName);

	// Update sounds
	void Update();

};


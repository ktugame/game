#include "SoundManager.h"
#include "ContentLoader.h"

SoundManager::SoundManager(ContentLoader *GameContent)
{
	this->GameContent = GameContent;
}

SoundManager::~SoundManager()
{
	auto Size = Sounds.size();

	for (size_t i = 0; i < Size; i++)
	{
		auto Sound = Sounds[i];

		if (Sound)
		{
			Sounds[i]->Sound.stop();
			Sounds[i]->StartedToPlay = true;
		}
	}

	for (size_t i = 0; i < Size; i++)
	{
		auto Sound = Sounds[i];

		if (Sound)
		{
			delete Sound;
			Sounds[i] = nullptr;
			Sound = nullptr;
		}
	}
}

void SoundManager::Play(string SoundName)
{
	if (Sounds.size() >= 200) return;

	auto buffer = GameContent->GetSoundBuffers()[SoundName];

	if (!buffer) return;

	SoundStatus *status = new SoundStatus();

	status->Sound.setBuffer(*buffer);

	status->Sound.setVolume(15.0f);

	status->StartedToPlay = false;

	Sounds.push_back(status);
}

void SoundManager::Update()
{
	for (size_t i = 0; i < Sounds.size(); i++)
	{
		auto Sound = Sounds[i];

		if (!Sound) continue;

		if (!Sound->StartedToPlay)
		{
			Sound->StartedToPlay = true;
			Sound->Sound.play();
		}
		else
		{
			if (Sound->Sound.getStatus() == sf::SoundSource::Status::Stopped)
			{
				delete Sound;

				Sounds[i] = nullptr;
				Sound = nullptr;

				Sounds.erase(Sounds.begin() + i);

				if (i > 0)
				{
					i--;
				}
				else
				{
					i = 0;
				}
			}
		}
	}
}

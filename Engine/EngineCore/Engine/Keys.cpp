﻿#include "Keys.h"

#ifdef OS_WINDOWS
std::map<std::string, int>
#elif OS_LINUX
std::map<std::string, sf::Keyboard::Key>
#endif

Keys::Key = {
	{ "A", INPUT_KEY_A },
	{ "B", INPUT_KEY_B },
	{ "C", INPUT_KEY_C },
	{ "D", INPUT_KEY_D },
	{ "E", INPUT_KEY_E },
	{ "F", INPUT_KEY_F },
	{ "G", INPUT_KEY_G },
	{ "H", INPUT_KEY_H },
	{ "I", INPUT_KEY_I },
	{ "J", INPUT_KEY_J },
	{ "K", INPUT_KEY_K },
	{ "L", INPUT_KEY_L },
	{ "M", INPUT_KEY_M },
	{ "N", INPUT_KEY_N },
	{ "O", INPUT_KEY_O },
	{ "P", INPUT_KEY_P },
	{ "Q", INPUT_KEY_Q },
	{ "R", INPUT_KEY_R },
	{ "S", INPUT_KEY_S },
	{ "T", INPUT_KEY_T },
	{ "U", INPUT_KEY_U },
	{ "V", INPUT_KEY_V },
	{ "W", INPUT_KEY_W },
	{ "X", INPUT_KEY_X },
	{ "Y", INPUT_KEY_Y },
	{ "Z", INPUT_KEY_Z },

	{ "a", INPUT_KEY_A },
	{ "b", INPUT_KEY_B },
	{ "c", INPUT_KEY_C },
	{ "d", INPUT_KEY_D },
	{ "e", INPUT_KEY_E },
	{ "f", INPUT_KEY_F },
	{ "g", INPUT_KEY_G },
	{ "h", INPUT_KEY_H },
	{ "i", INPUT_KEY_I },
	{ "j", INPUT_KEY_J },
	{ "k", INPUT_KEY_K },
	{ "l", INPUT_KEY_L },
	{ "m", INPUT_KEY_M },
	{ "n", INPUT_KEY_N },
	{ "o", INPUT_KEY_O },
	{ "p", INPUT_KEY_P },
	{ "q", INPUT_KEY_Q },
	{ "r", INPUT_KEY_R },
	{ "s", INPUT_KEY_S },
	{ "t", INPUT_KEY_T },
	{ "u", INPUT_KEY_U },
	{ "v", INPUT_KEY_V },
	{ "w", INPUT_KEY_W },
	{ "x", INPUT_KEY_X },
	{ "y", INPUT_KEY_Y },
	{ "z", INPUT_KEY_Z },

	{ "0", INPUT_KEY_0 },
	{ "1", INPUT_KEY_1 },
	{ "2", INPUT_KEY_2 },
	{ "3", INPUT_KEY_3 },
	{ "4", INPUT_KEY_4 },
	{ "5", INPUT_KEY_5 },
	{ "6", INPUT_KEY_6 },
	{ "7", INPUT_KEY_7 },
	{ "8", INPUT_KEY_8 },
	{ "9", INPUT_KEY_9 },

	{ "F1", INPUT_KEY_F1 },
	{ "F2", INPUT_KEY_F2 },
	{ "F3", INPUT_KEY_F3 },
	{ "F4", INPUT_KEY_F4 },
	{ "F5", INPUT_KEY_F5 },
	{ "F6", INPUT_KEY_F6 },
	{ "F7", INPUT_KEY_F7 },
	{ "F8", INPUT_KEY_F8 },
	{ "F9", INPUT_KEY_F9 },
	{ "F10", INPUT_KEY_F10 },
	{ "F11", INPUT_KEY_F11 },
	{ "F12", INPUT_KEY_F12 },

	{ "`", INPUT_KEY_OEM_3 },
	{ "~", INPUT_KEY_OEM_3 },

	{ "lshift", INPUT_KEY_LSHIFT },

	{ "esc", INPUT_KEY_ESC },

};

#ifdef OS_WINDOWS

int Keys::GetKeyboardKey(std::string key)
{
	std::map<std::string, int>::iterator iterator = Key.find(key);

	if (iterator != Key.end())
	{
		return iterator->second;
	}
	else
	{
		return 0;
	}
}

#elif OS_LINUX

sf::Keyboard::Key Keys::GetKeyboardKey(std::string key)
{
	std::map<std::string, sf::Keyboard::Key>::iterator iterator = Key.find(key);

	if (iterator != Key.end())
	{
		return iterator->second;
	}
	else
	{
		return sf::Keyboard::Unknown;
	}
}

#endif

#include "Settings.h"
#include <map>

#ifdef OS_WINDOWS
const string INI_FILE = "\\settings.ini";
#elif OS_LINUX
const string INI_FILE = "/settings.ini";
#endif

#ifdef OS_WINDOWS
Settings::Settings()
#elif OS_LINUX
Settings::Settings(char *arg)
#endif
{
	ResetSettings();

#ifdef OS_WINDOWS

	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	filePath = buffer;

#elif OS_LINUX

	this->filePath = arg;

#endif

	filePath = filePath.substr(0, filePath.find_last_of("/\\"));
	filePath = filePath + INI_FILE;

	LoadFromFile();
}

void Settings::ResetSettings()
{
	this->windowSize.x = 1280;
	this->windowSize.y = 720;
	this->framesPerSecond = -1;
	this->windowPosition.x = -1;
	this->windowPosition.y = -1;
	this->antialiasingLevel = 0;
	this->isFullScreen = false;
	this->isOnVerticalSync = false;
	this->showFps = false;
}

void Settings::WriteToFile()
{
	using std::fstream;
	using std::endl;

	fstream settingsFile(toCharArr(filePath), std::fstream::out);

	// TODO: add error why file is not opened
	if (!settingsFile.is_open())
		return;

	settingsFile
		//<< "[WINDOW]" << endl
		<< "width=" << this->windowSize.x << endl
		<< "height=" << this->windowSize.y << endl
		<< "fullscreen=" << this->isFullScreen << endl
		<< "x_pos=" << this->windowPosition.x << endl
		<< "y_pos=" << this->windowPosition.y << endl;

	settingsFile
		//<< "[ENGINE]" << endl
		<< "fps=" << this->framesPerSecond << endl
		<< "antialiasing=" << this->antialiasingLevel << endl
		<< "vsync=" << this->isOnVerticalSync << endl
		<< "show_fps=" << this->showFps << endl;

	settingsFile
		//<< "[CONTROLS]"
		<< endl;

	settingsFile.close();
}

void Settings::LoadFromFile()
{
	using std::ifstream;
	using std::map;

	ifstream settingsFile(toCharArr(filePath));

	// TODO: add error why file is not opened
	if (!settingsFile.is_open())
		return;

	// TODO: optimize (memory, speed)
	// TODO: research or do benchmark: who is faster <string.find> or <strstr>?
	string line;
	while(std::getline(settingsFile, line))
	{
		if (line.find("width") != std::string::npos)
		{
			windowSize.x =
					static_cast<uint>(std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()));
		}
		else if (line.find("height") != std::string::npos)
		{
			windowSize.y =
					static_cast<uint>(std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()));
		}
		else if (line.find("fullscreen") != std::string::npos)
		{
			// TODO: expression can be simplified
			isFullScreen =
					std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()) > 0 ? true : false;
		}
		else if (line.find("x_pos") != std::string::npos)
		{
			windowPosition.x =
					static_cast<int>(std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()));
		}
		else if (line.find("y_pos") != std::string::npos)
		{
			windowPosition.y =
					static_cast<int>(std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()));
		}
		else if (line.find("fps") != std::string::npos)
		{
			framesPerSecond =
					static_cast<int>(std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()));
		}
		else if (line.find("antialiasing") != std::string::npos)
		{
			antialiasingLevel =
					static_cast<uint>(std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()));
		}
		else if (line.find("vsync") != std::string::npos)
		{
			// TODO: expression can be simplified
			isOnVerticalSync =
					std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()) > 0 ? true : false;
		}
		else if (line.find("show_fps") != std::string::npos)
		{
			// TODO: expression can be simplified
			showFps = std::atoi(line.substr(line.find('=') + 1, line.size()).c_str()) > 0 ? true : false;
		}
	}

	settingsFile.close();
}

Settings::~Settings()
{
	this->WriteToFile();
}

#ifdef DEBUG

#include <iostream>

void Settings::PrintVariables()
{
	using std::cout;
	using std::endl;

	cout << "Settings class variables" << endl;
	cout << "windowWidth=" << windowSize.x << endl;
	cout << "windowHeight=" << windowSize.y << endl;
	cout << "framesPerSecond=" << framesPerSecond << endl;
	cout << "windowPositionX=" << windowPosition.x << endl;
	cout << "windowPositionY=" << windowPosition.y << endl;
	cout << "antialiasingLevel=" << antialiasingLevel << endl;
	cout << "isFullScreen=" << isFullScreen << endl;
	cout << "isOnVerticalSync=" << isOnVerticalSync << endl;
	cout << "showFps=" << showFps << endl;
}

#endif

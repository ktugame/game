#pragma once

#include <SFML/Graphics.hpp>

// Class for player or object fallowing
// Or just displaying (UI) things on the screen
class Camera
{
protected:

	// Get mouse position on the screen
	sf::Vector2i *MousePositionOnScreen;

	// sf::Renderwindow for accessing renderer and window->draw() function
	sf::RenderWindow *GameWindow;

	// Zoom value
	float ZoomValue = 1.0f;

public:

	// Constructor
	// GameWindow - renderer
	// HookVector - vector for hooking
	// MousePositionOnScreen - mouse pos on the scree
	Camera(sf::RenderWindow *GameWindow, sf::Vector2i *MousePositionOnScreen);

	// Descturctor
	~Camera();

	// Updating camera
	void Update(const float Tick, const sf::Vector2f *Position = nullptr);

	// GameViewPort - viso action'o camera (keicia pozicija)
	// UIViewPort - viso UI camera (nekeicia pozicijos)
	// Info:
	// https://www.sfml-dev.org/tutorials/2.0/graphics-view.php
	sf::View *GameViewPort, *UIViewPort;

	// Change zoom
	void ChangeZoom_Delta(float val);

	// Reset zoom
	void ResetZoom();

	// Get view size
	sf::Vector2i GetViewSize();

private:

	// State of the hook
	bool bHooked;

public:

	// Grazina current state, ar hookinta ar ne
	void ToggleHook();

};

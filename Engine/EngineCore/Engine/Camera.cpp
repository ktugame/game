#include "Camera.h"

Camera::Camera(sf::RenderWindow *GameWindow, sf::Vector2i *MousePositionOnScreen)
{
	this->GameViewPort = nullptr;
	this->GameWindow = GameWindow;
	this->MousePositionOnScreen = MousePositionOnScreen;

	if (GameWindow)
	{
		GameViewPort = new sf::View(
			sf::Vector2f(0, 0),
			sf::Vector2f(GameWindow->getView().getSize())
		);

		UIViewPort = new sf::View(
			sf::Vector2f(0, 0),
			sf::Vector2f(GameWindow->getView().getSize())
		);

		GameWindow->setView(*GameViewPort);
	}

#ifdef DEBUG
	this->bHooked = true;
#endif // !DEBUG
}

Camera::~Camera()
{
	if (GameViewPort)
	{
		delete GameViewPort;
		this->GameViewPort = nullptr;
	}

	if (UIViewPort)
	{
		delete UIViewPort;
		this->UIViewPort = nullptr;
	}

	this->GameWindow = nullptr;
	this->MousePositionOnScreen = nullptr;
}

sf::Vector2i Camera::GetViewSize()
{
	return {
		static_cast<int>(GameWindow->getSize().x),
		static_cast<int>(GameWindow->getSize().y) };
}

void Camera::Update(const float Tick, const sf::Vector2f *Position)
{
	if (GameViewPort)
	{
		sf::Vector2f GameWinSize = sf::Vector2f(
			static_cast<float>(GameWindow->getSize().x),
			static_cast<float>(GameWindow->getSize().y));

		GameViewPort->setSize(GameWinSize);
		UIViewPort->setSize(GameWinSize);
		GameViewPort->zoom(ZoomValue);

#ifdef DEBUG
#define CORNER_LENGHT 100
#define CAMERA_SPEED 800.0f
		if (Position && bHooked)
		{
			GameViewPort->setCenter(*Position);
		}
		else
		{
			if (MousePositionOnScreen)
			{
				if (!(MousePositionOnScreen->x < 0) && !(MousePositionOnScreen->x > GameViewPort->getSize().x) &&
					!(MousePositionOnScreen->y < 0) && !(MousePositionOnScreen->y > GameViewPort->getSize().y))
				{
					float MoveBy = Tick * CAMERA_SPEED;

					if (MousePositionOnScreen->x < CORNER_LENGHT)
					{
						GameViewPort->setCenter(
							GameViewPort->getCenter() +
							sf::Vector2f(-MoveBy * (CORNER_LENGHT - MousePositionOnScreen->x) / CORNER_LENGHT, 0)
						);
					}
					else if (MousePositionOnScreen->x > GameViewPort->getSize().x - CORNER_LENGHT)
					{
						GameViewPort->setCenter(
							GameViewPort->getCenter() +
							sf::Vector2f(-MoveBy * (GameViewPort->getSize().x - CORNER_LENGHT - MousePositionOnScreen->x) / CORNER_LENGHT, 0)
						);
					}

					if (MousePositionOnScreen->y < CORNER_LENGHT)
					{
						GameViewPort->setCenter(
							GameViewPort->getCenter() +
							sf::Vector2f(0, -MoveBy * (CORNER_LENGHT - MousePositionOnScreen->y) / CORNER_LENGHT)
						);
					}
					else if (MousePositionOnScreen->y > GameViewPort->getSize().y - CORNER_LENGHT)
					{
						GameViewPort->setCenter(
							GameViewPort->getCenter() +
							sf::Vector2f(0, -MoveBy * (GameViewPort->getSize().y - CORNER_LENGHT - MousePositionOnScreen->y) / CORNER_LENGHT)
						);
					}
				}
			}
		}
#else
		if (Position)
		{
			GameViewPort->setCenter(*Position);
		}
#endif // !DEBUG

		GameWindow->setView(*GameViewPort);

	}
}

void Camera::ChangeZoom_Delta(float val)
{
	float level = ZoomValue + val;

	if (!(level < 0.15 || level > 5))
	{
		ZoomValue = level;
	}
}

void Camera::ResetZoom()
{
	ZoomValue = 1.0f;
}

#ifdef DEBUG

void Camera::ToggleHook()
{
	bHooked = !bHooked;
}

#endif // DEBUG

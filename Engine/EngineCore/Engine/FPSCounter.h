#pragma once

#include <chrono>

// FPS and Tick counter structure
struct FPSCounter
{
	using CHRONO = std::chrono::time_point<std::chrono::steady_clock>;
	using CLOCK = std::chrono::steady_clock;

	// Counter constructor
	FPSCounter()
	{
		Start = CLOCK::now();
		End = CLOCK::now();

		FPS = 0;
		Tick = 0;
		Time = 0;
		FrameCount = 0;
		ElapsedTime = 0;
	}

	// Counter destructor
	~FPSCounter()
	{

	}

	// Calculate Tick and Timers
	void CalcDiff()
	{
		End = CLOCK::now();

		Tick = std::chrono::duration <float, std::milli>(End - Start).count() / 1000;

		Start = CLOCK::now();

		if (Time >= 1.0f)
		{
			FPS = FrameCount / Time;
			FrameCount = 0;
			Time = 0;
		}
		Time += Tick;

		ElapsedTime += Tick;
		FrameCount++;
	}

	// Get Tick
	float GetTick()
	{
		return Tick;
	}

	// Get FPS as float
	float GetFPS_float()
	{
		return FPS;
	}

	// Get FPS as int
	int GetFPS_int()
	{
		return static_cast<int>(FPS);
	}

	// Get Elapsed time
	float GetElapsedTime_float()
	{
		return ElapsedTime;
	}

	int GetElapsedTime_int()
	{
		return static_cast<int>(ElapsedTime);
	}

	void ResetTimer()
	{
		ElapsedTime = 0;
	}

private:

	// Chrono time start
	CHRONO Start;

	// Chrono time end
	CHRONO End;

	// Elapsed time as seconds
	float Tick;

	// Frame count
	int FrameCount;

	// Getting elapsed time
	float Time;

	// Frames count / Time
	float FPS;

	// Elapsed game time
	float ElapsedTime;
};

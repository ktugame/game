#pragma once

#include <map>
#include <string>

#ifdef OS_WINDOWS
#include <Windows.h>
#elif OS_LINUX
#include <SFML/Window/Keyboard.hpp>
#endif

// Map - key->value ordered array
struct Keys
{
public:

#ifdef OS_WINDOWS

public:

	// Get key by char key
	static int GetKeyboardKey(std::string key);

private:

	// Static map list for keys
	static std::map<std::string, int> Key;

#elif OS_LINUX

public:

	// Get key by char key
	static sf::Keyboard::Key GetKeyboardKey(std::string key);

private:

	// Static map list for keys
	static std::map<std::string, sf::Keyboard::Key> Key;

#endif
};


#ifdef OS_WINDOWS

// https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx

#define INPUT_KEY_A 0x41
#define INPUT_KEY_B 0x42
#define INPUT_KEY_C 0x43
#define INPUT_KEY_D 0x44
#define INPUT_KEY_E 0x45
#define INPUT_KEY_F 0x46
#define INPUT_KEY_G 0x47
#define INPUT_KEY_H 0x48
#define INPUT_KEY_I 0x49
#define INPUT_KEY_J 0x4A
#define INPUT_KEY_K 0x4B
#define INPUT_KEY_L 0x4C
#define INPUT_KEY_M 0x4D
#define INPUT_KEY_N 0x4E
#define INPUT_KEY_O 0x4F
#define INPUT_KEY_P 0x50
#define INPUT_KEY_Q 0x51
#define INPUT_KEY_R 0x52
#define INPUT_KEY_S 0x53
#define INPUT_KEY_T 0x54
#define INPUT_KEY_U 0x55
#define INPUT_KEY_V 0x56
#define INPUT_KEY_W 0x57
#define INPUT_KEY_X 0x58
#define INPUT_KEY_Y 0x59
#define INPUT_KEY_Z 0x5A

#define INPUT_KEY_0 0x30
#define INPUT_KEY_1 0x31
#define INPUT_KEY_2 0x32
#define INPUT_KEY_3 0x33
#define INPUT_KEY_4 0x34
#define INPUT_KEY_5 0x35
#define INPUT_KEY_6 0x36
#define INPUT_KEY_7 0x37
#define INPUT_KEY_8 0x38
#define INPUT_KEY_9 0x39

#define INPUT_KEY_F1 VK_F1
#define INPUT_KEY_F2 VK_F2
#define INPUT_KEY_F3 VK_F3
#define INPUT_KEY_F4 VK_F4
#define INPUT_KEY_F5 VK_F5
#define INPUT_KEY_F6 VK_F6
#define INPUT_KEY_F7 VK_F7
#define INPUT_KEY_F8 VK_F8
#define INPUT_KEY_F9 VK_F9
#define INPUT_KEY_F10 VK_F10
#define INPUT_KEY_F11 VK_F11
#define INPUT_KEY_F12 VK_F12

#define INPUT_KEY_OEM_3 VK_OEM_3
#define INPUT_KEY_LSHIFT VK_LSHIFT

#define INPUT_KEY_ESC VK_ESCAPE


#elif OS_LINUX

#include <SFML/Window/Keyboard.hpp>

#define INPUT_KEY_A sf::Keyboard::A
#define INPUT_KEY_B sf::Keyboard::B
#define INPUT_KEY_C sf::Keyboard::C
#define INPUT_KEY_D sf::Keyboard::D
#define INPUT_KEY_E sf::Keyboard::E
#define INPUT_KEY_F sf::Keyboard::F
#define INPUT_KEY_G sf::Keyboard::G
#define INPUT_KEY_H sf::Keyboard::H
#define INPUT_KEY_I sf::Keyboard::I
#define INPUT_KEY_J sf::Keyboard::J
#define INPUT_KEY_K sf::Keyboard::K
#define INPUT_KEY_L sf::Keyboard::L
#define INPUT_KEY_M sf::Keyboard::M
#define INPUT_KEY_N sf::Keyboard::N
#define INPUT_KEY_O sf::Keyboard::O
#define INPUT_KEY_P sf::Keyboard::P
#define INPUT_KEY_Q sf::Keyboard::Q
#define INPUT_KEY_R sf::Keyboard::R
#define INPUT_KEY_S sf::Keyboard::S
#define INPUT_KEY_T sf::Keyboard::T
#define INPUT_KEY_U sf::Keyboard::U
#define INPUT_KEY_V sf::Keyboard::V
#define INPUT_KEY_W sf::Keyboard::W
#define INPUT_KEY_X sf::Keyboard::X
#define INPUT_KEY_Y sf::Keyboard::Y
#define INPUT_KEY_Z sf::Keyboard::Z

#define INPUT_KEY_0 sf::Keyboard::Num0
#define INPUT_KEY_1 sf::Keyboard::Num1
#define INPUT_KEY_2 sf::Keyboard::Num2
#define INPUT_KEY_3 sf::Keyboard::Num3
#define INPUT_KEY_4 sf::Keyboard::Num4
#define INPUT_KEY_5 sf::Keyboard::Num5
#define INPUT_KEY_6 sf::Keyboard::Num6
#define INPUT_KEY_7 sf::Keyboard::Num7
#define INPUT_KEY_8 sf::Keyboard::Num8
#define INPUT_KEY_9 sf::Keyboard::Num9

#define INPUT_KEY_F1 sf::Keyboard::F1
#define INPUT_KEY_F2 sf::Keyboard::F2
#define INPUT_KEY_F3 sf::Keyboard::F3
#define INPUT_KEY_F4 sf::Keyboard::F4
#define INPUT_KEY_F5 sf::Keyboard::F5
#define INPUT_KEY_F6 sf::Keyboard::F6
#define INPUT_KEY_F7 sf::Keyboard::F7
#define INPUT_KEY_F8 sf::Keyboard::F8
#define INPUT_KEY_F9 sf::Keyboard::F9
#define INPUT_KEY_F10 sf::Keyboard::F10
#define INPUT_KEY_F11 sf::Keyboard::F11
#define INPUT_KEY_F12 sf::Keyboard::F12

#define INPUT_KEY_OEM_3 sf::Keyboard::Tilde
#define INPUT_KEY_LSHIFT sf::Keyboard::LShift

#define INPUT_KEY_ESC sf::Keyboard::Escape

#endif

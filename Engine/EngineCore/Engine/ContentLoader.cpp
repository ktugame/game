#include "ContentLoader.h"
#include "DEBUG.h"

#ifdef OS_WINDOWS

ContentLoader::ContentLoader()
{
#ifdef DEBUG
	MESSAGE("ContentLoader::ContentLoader()");
#endif
	// Extract path
	// Windows method

	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	Path = buffer;
	Path = Path.substr(0, Path.find_last_of("/\\"));

	IsDataLoaded = false;
}

#elif OS_LINUX

ContentLoader::ContentLoader(string Path)
{
}

#endif // OS_WINDOWS

ContentLoader::~ContentLoader()
{
#ifdef DEBUG
	MESSAGE("ContentLoader::~ContentLoader() - START");
#endif

	// Deleting fonts
	{
		// Iteratorius, kad perziureti elementus ir pasiekti "first/second" key'us be ju pavadinimu
		GmaeFonts::iterator iterator;

		// Pradedam nuo pradzios ir einam link pabaigos ir ++
		for (iterator = Fonts.begin(); iterator != Fonts.end(); ++iterator)
		{
			// Deletinam memory kuri laiko (gavusi is pointerio anksciau)
			delete iterator->second;

			iterator->second = nullptr;
		}

		Fonts.clear();
	}

	// Sounds fonts
	{
		GameSoundBuffers::iterator iterator;

		for (iterator = SoundBuffers.begin(); iterator != SoundBuffers.end(); ++iterator)
		{
			delete iterator->second;

			iterator->second = nullptr;
		}

		SoundBuffers.clear();
	}

	// Textures fonts
	{
		GameTextures::iterator iterator;

		for (iterator = Textures.begin(); iterator != Textures.end(); ++iterator)
		{
			delete iterator->second;

			iterator->second = nullptr;
		}

		Textures.clear();
	}

#ifdef DEBUG
	MESSAGE("ContentLoader::~ContentLoader() - END");
#endif
}

#ifdef OS_WINDOWS

void ContentLoader::LoadData()
{
#ifdef DEBUG
	MESSAGE("ContentLoader::LoadData()");
#endif

	if (IsDataLoaded) return;

	bool Status = true;

	using MAP_LIST = std::map<string, string>;

	// Loading fonts
	{
		MAP_LIST FontMap = {
			{ "OswaldRegular", "\\Fonts\\Oswald-Regular.ttf" },
			{ "OswaldMedium", "\\Fonts\\Oswald-Medium.ttf" },
			{ "OswaldBold", "\\Fonts\\Oswald-Bold.ttf" },
		};

		sf::Font *Font;

		MAP_LIST::iterator iterator = FontMap.begin();

		for (; iterator != FontMap.end(); ++iterator)
		{
			Font = new sf::Font();
			Font->loadFromFile(Path + iterator->second) ? Status : Status = false;
			Fonts[iterator->first] = Font;
		}
	}

	// Loading texutres
	{
		MAP_LIST TextureMap = {
			{ "MapBackground", "\\Graphics\\Background00.jpg" },
			{ "WallTile", "\\Graphics\\WallTile.png" },

			{ "Blood00", "\\Graphics\\Blood00.png" },
			{ "Blood01", "\\Graphics\\Blood01.png" },
			{ "Blood02", "\\Graphics\\Blood02.png" },
			{ "Blood03", "\\Graphics\\Blood03.png" },
			{ "Blood04", "\\Graphics\\Blood04.png" },
			{ "Blood05", "\\Graphics\\Blood05.png" },
			{ "Blood06", "\\Graphics\\Blood06.png" },
			//{ "Blood07", "\\Graphics\\Blood07.png" },
			{ "Blood08", "\\Graphics\\Blood18.png" },
			{ "Blood09", "\\Graphics\\Blood19.png" },
			{ "Blood10", "\\Graphics\\Blood20.png" },

			{ "EnemySpawner", "\\Graphics\\SpawnerEnemy.png" },
			{ "EnemyBug", "\\Graphics\\EnemyBug.png" },
			{ "EnemySlime", "\\Graphics\\Slime.png" },

			{ "Pickup", "\\Graphics\\Pickup.png" },

			{ "Projectiles", "\\Graphics\\Projectiles.png" },

			{ "PlayerPistol", "\\Graphics\\PlayerPistol.png" },
			{ "PlayerRifle", "\\Graphics\\PlayerRifle.png" },

			{ "WDesertEagle", "\\Graphics\\WDesertEagle.png" },
			{ "WPistol1", "\\Graphics\\WPistol1.png" },
			{ "WMossberg590", "\\Graphics\\WShotgun1.png" },

		};

		sf::Texture *Texture = nullptr;

		MAP_LIST::iterator iterator = TextureMap.begin();

		for (; iterator != TextureMap.end(); ++iterator)
		{
			Texture = new sf::Texture();
			Texture->loadFromFile(Path + iterator->second) ? Status : Status = false;
			Texture->setSmooth(true);
			Textures[iterator->first] = Texture;
		}
	}

	// Loading sounds
	{
		MAP_LIST SoundMap = {
			{ "PistolShot1", "\\Sounds\\pistol_shot1.wav" },
			{ "PistolShot2", "\\Sounds\\pistol_shot2.wav" },
			{ "ShotgunPump1", "\\Sounds\\shotgun_pump1.wav" },
			{ "ShotgunPump2", "\\Sounds\\shotgun_pump2.wav" },
			{ "ShotgunShot1", "\\Sounds\\shotgun_shot1.wav" },
			{ "ShotgunShot2", "\\Sounds\\shotgun_shot2.wav" },
			{ "ShotgunShot3", "\\Sounds\\shotgun_shot3.wav" },
		};

		sf::SoundBuffer *Sound = nullptr;

		MAP_LIST::iterator iterator = SoundMap.begin();

		for (; iterator != SoundMap.end(); ++iterator)
		{
			Sound = new sf::SoundBuffer();
			Sound->loadFromFile(Path + iterator->second) ? Status : Status = false;
			SoundBuffers[iterator->first] = Sound;
		}
	}

	IsDataLoaded = true;
}

#elif OS_LINUX

void ContentLoader::LoadData()
{
#ifdef DEBUG
	MESSAGE("ContentLoader::LoadData()");
#endif


	using MAP_LIST = std::map<string, string>;

	// Loading fonts
	{
		MAP_LIST FontMap = {
				{ "OswaldRegular", 	"Fonts/Oswald-Regular.ttf" },
				{ "OswaldMedium", 	"Fonts/Oswald-Medium.ttf" },
				{ "OswaldBold", 	"Fonts/Oswald-Bold.ttf" },
		};

		sf::Font *Font;

		MAP_LIST::iterator iterator = FontMap.begin();

		for (; iterator != FontMap.end(); ++iterator)
		{
			Font = new sf::Font();
			Font->loadFromFile(Path + iterator->second);
			Fonts[iterator->first] = Font;
		}
	}

	// Loading texutres
	{
		MAP_LIST TextureMap = {
				{ "MapBackground", 	"Graphics/Background00.jpg" },
				{ "WallTile", 		"Graphics/WallTile.png" },

				{ "Blood00", "Graphics/Blood00.png" },
				{ "Blood01", "Graphics/Blood01.png" },
				{ "Blood02", "Graphics/Blood02.png" },
				{ "Blood03", "Graphics/Blood03.png" },
				{ "Blood04", "Graphics/Blood04.png" },
				{ "Blood05", "Graphics/Blood05.png" },
				{ "Blood06", "Graphics/Blood06.png" },
				{ "Blood08", "Graphics/Blood18.png" },
				{ "Blood09", "Graphics/Blood19.png" },
				{ "Blood10", "Graphics/Blood20.png" },

				{ "EnemySpawner", 	"Graphics/SpawnerEnemy.png" },
				{ "EnemyBug", 		"Graphics/EnemyBug.png" },
				{ "EnemySlime", 	"Graphics/Slime.png" },

				{ "Pickup", 		"Graphics/Pickup.png" },

				{ "Projectiles", 	"Graphics/Projectiles.png" },

				{ "PlayerPistol", 	"Graphics/PlayerPistol.png" },
				{ "PlayerRifle", 	"Graphics/PlayerRifle.png" },

				{ "WDesertEagle", 	"Graphics/WDesertEagle.png" },
				{ "WPistol1", 		"Graphics/WPistol1.png" },
				{ "WMossberg590", 	"Graphics/WShotgun1.png" },

		};

		sf::Texture *Texture = nullptr;

		MAP_LIST::iterator iterator = TextureMap.begin();

		for (; iterator != TextureMap.end(); ++iterator)
		{
			Texture = new sf::Texture();
			Texture->loadFromFile(Path + iterator->second);
			Texture->setSmooth(true);
			Textures[iterator->first] = Texture;
		}
	}

	// Loading sounds
	{
		MAP_LIST SoundMap = {
				{ "PistolShot1", 	"Sounds/pistol_shot1.wav" },
				{ "PistolShot2", 	"Sounds/pistol_shot2.wav" },
				{ "ShotgunPump1", 	"Sounds/shotgun_pump1.wav" },
				{ "ShotgunPump2", 	"Sounds/shotgun_pump2.wav" },
				{ "ShotgunShot1", 	"Sounds/shotgun_shot1.wav" },
				{ "ShotgunShot2", 	"Sounds/shotgun_shot2.wav" },
				{ "ShotgunShot3", 	"Sounds/shotgun_shot3.wav" },
		};

		sf::SoundBuffer *Sound = nullptr;

		MAP_LIST::iterator iterator = SoundMap.begin();

		for (; iterator != SoundMap.end(); ++iterator)
		{
			Sound = new sf::SoundBuffer();
			Sound->loadFromFile(Path + iterator->second);
			SoundBuffers[iterator->first] = Sound;
		}

	}

}

#endif // OS_WINDOWS

string ContentLoader::GetDataPath()
{
	return Path;
}

GmaeFonts &ContentLoader::GetFonts()
{
	return this->Fonts;
}

GameSoundBuffers &ContentLoader::GetSoundBuffers()
{
	return this->SoundBuffers;
}

GameTextures &ContentLoader::GetTextures()
{
	return this->Textures;
}

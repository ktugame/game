#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <map>
#include <set>

using std::string;
using GmaeFonts = std::map<string, sf::Font*>;
using GameSoundBuffers = std::map<string, sf::SoundBuffer*>;
using GameTextures = std::map<string, sf::Texture*>;

// Content loader class
// Loads all the content that game requires
class ContentLoader
{
private:

	// Font set
	GmaeFonts Fonts;

	// Sound buffer set
	GameSoundBuffers SoundBuffers;

	// Texture set
	GameTextures Textures;

	// Exe/Game data/Resources directory 
	string Path;

public:

#ifdef OS_WINDOWS
	// Constructor
	ContentLoader();
#elif OS_LINUX
	ContentLoader(string Path);
#endif

	// Destructor
	~ContentLoader();

	// Load data
	void LoadData();

	// Gets path to resources
	string GetDataPath();

	// Get fonts set pointer
	GmaeFonts &GetFonts();

	// Get sound buffers set pointer
	GameSoundBuffers &GetSoundBuffers();

	// Get textures set pointer
	GameTextures &GetTextures();

};

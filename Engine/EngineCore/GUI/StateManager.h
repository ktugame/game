#pragma once

#ifdef OS_WINDOWS
#include <Windows.h>
#endif
#include <thread>
#include "LoadingView.h"

using Function = std::function<void(void)>;
using Thread = std::thread;

class GameEngine;
class ContentLoader;
class StateInGame;

class StateManager
{
private:

	// Loading view
	LoadingView *Loading;

	// GameEngine pointer
	GameEngine *Engine;

	StateInGame *CurrentGameState;

	// Current game state and next game state
	//GameStateAbstract *CurrentGameState;

public:

	// State constructor
	StateManager(GameEngine *Engine);

	// State destructor
	~StateManager();

	// Give ready status
	void Ready();

};

#include "UIText.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"

UIText::UIText(GameEngine *Engine, string Text, sf::Vector2f Position)
{
	this->Engine = Engine;
	this->Text.setString(Text);
	//this->Text.setFont(*Engine->GameContent->GetFonts()["OswaldRegular"]);
	this->TargetWindowResolution = &Engine->TargetWindowResolution;

	SetPosition(Position);

	this->SetColor(sf::Color({ 255,255,255, 200 }));
}

UIText::~UIText()
{

}

void UIText::draw(sf::RenderTarget &Render, sf::RenderStates States) const
{
	Render.draw(Text);
}

void UIText::Update(const float Tick)
{

}

void UIText::SetText(string Text)
{
	this->Text.setString(Text);
}

void UIText::SetFontSize(unsigned int FontSize)
{
	Text.setCharacterSize(FontSize);
}

void UIText::SetPosition(sf::Vector2f Position)
{
	if (!Engine || !TargetWindowResolution) return;

	//auto View = Engine->GameWindow->getDefaultView().getSize();
	auto View = sf::View().getSize();
	auto Rez = Engine->TargetWindowResolution;

	float xRatio = View.x / TargetWindowResolution->x;
	float yRatio = View.y / TargetWindowResolution->y;

	Position.x *= xRatio;
	Position.y *= yRatio;

	Text.setPosition(Position);
}

void UIText::SetFont(const sf::Font *Font)
{
	if (Font)
	{
		this->Text.setFont(*Font);
	}
}

void UIText::SetColor(sf::Color Color)
{
	//this->Text.setFillColor(Color);
}

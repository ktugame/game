#include "UIManager.h"
#include "../Engine/DEBUG.h"
#include "Widget.h"

UIManager::UIManager()
{
#ifdef DEBUG
	MESSAGE("UIManager::UIManager()");
#endif
	this->UIView = nullptr;
}

UIManager::~UIManager()
{
#ifdef DEBUG
	MESSAGE("UIManager::~UIManager()");
#endif
	ClearWidgets();
}

void UIManager::AddWidget(WName Name, Widget *widget)
{
#ifdef DEBUG
	MESSAGE("UIManager::AddWidget(WName Name, Widget &widget)");
#endif

	Widgets.insert({ Name, widget });
}

void UIManager::ClearWidgets()
{
	for (auto iterator = Widgets.begin(); iterator != Widgets.end(); )
	{
		delete iterator->second;
		iterator->second = nullptr;
		iterator = Widgets.erase(iterator);
	}
	Widgets.clear();
}

void UIManager::Ready(sf::View *UIView)
{
	this->UIView = UIView;
}

void UIManager::Update(const float Tick)
{
	for (auto iterator = Widgets.begin(); iterator != Widgets.end(); iterator++)
	{
		iterator->second->Update(Tick);
	}
}

void UIManager::draw(sf::RenderTarget &Render, sf::RenderStates States) const
{
	sf::View tempGameWindowView = Render.getView();

	if (UIView) Render.setView(*UIView);

	for (auto iterator = Widgets.begin(); iterator != Widgets.end(); iterator++)
	{
		Render.draw(*iterator->second, States);
	}

	Render.setView(tempGameWindowView);
}

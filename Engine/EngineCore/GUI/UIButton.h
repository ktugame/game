#pragma once

#include "UIElements.h"
#include <functional>

class ContentLoader;
class GameEngine;

using uint = unsigned int;

using Function = std::function<void(void)>;

// Migrukas, kuris su kazkuo interaktinsis, saugomas UIElements 
class UIButton : public UIElements
{
private:

	// VertexArrays
	sf::VertexArray VertexArr;

	// Function
	Function ButtonFunction;

	// Target Window Resolution
	const sf::Vector2i *TargetWindowResolution;

	// Default color
	const sf::Color DefaultColor = sf::Color(25, 25, 25, 180);

	// Hovered color
	const sf::Color HoverColor = sf::Color(10, 10, 10, 210);

	// Disabled default color
	const sf::Color DisabledColor = sf::Color(120, 120, 120, 200);

	// Current color of the button
	sf::Color CurrentColor = DefaultColor;

	// Button background
	sf::RectangleShape ButtonBackground;

	// Button text
	sf::Text Text;

	// Button shadow
	sf::Text TextShadow;

	// Text offset
	sf::Vector2f Offset;

	// Button position
	sf::Vector2f Position;

	// Button size
	sf::Vector2f HalfSize;

	GameEngine *Engine;

public:

	// Constructor
	UIButton();

	// Destructor
	~UIButton();

	// Update
	void Update(const float Tick);

	// Set button size
	void SetButtonSize(sf::Vector2f Size);

	// Set text size
	void SetTextSize(unsigned int Size);

	// Set position
	void SetTextOffsetPosition(sf::Vector2f Position);

	// set button positon
	void SetButtonPosition(sf::Vector2f Position);

	// Set Text
	void SetText(std::string Text);

	// Set function
	void SetFunction(Function CallFunction);

	// Get button size
	sf::Vector2f GetButtonHalfSize();

	// If button contais point
	bool ContainsPoint(sf::Vector2f Point);

protected:

	void draw(sf::RenderTarget &Render, sf::RenderStates States) const;

};

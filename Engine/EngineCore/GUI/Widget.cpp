#include "Widget.h"
#include "UIElements.h"
#include "../Engine/DEBUG.h"

Widget::Widget()
{
	this->bDestroy = false;
	this->bIsVisible = true;
}

Widget::~Widget()
{
	ClearUIElements();
}

void Widget::ClearUIElements()
{
	for (auto iterator = Elements.begin(); iterator != Elements.end();)
	{
		delete iterator->second;
		iterator->second = nullptr;
		iterator = Elements.erase(iterator);
	}
}

bool Widget::Destroy()
{
	return this->bDestroy;
}

void Widget::DestoyWidget()
{
	this->bDestroy = true;
}

UIElements *Widget::GetUIElement(UIName Name)
{
	auto iterator = Elements.find(Name);

	if (iterator != Elements.end())
	{
		return iterator->second;
	}

	return nullptr;
}

void Widget::AddUIElement(UIName Name, UIElements *element)
{
	Elements.insert({ Name, element });
}

bool Widget::IsVisible()
{
	return bIsVisible;
}

void Widget::ToggleVisibility()
{
	bIsVisible = !bIsVisible;
}

void Widget::Hide()
{
	bIsVisible = false;
}

void Widget::Show()
{
	bIsVisible = true;
}

#ifndef TESTING

void Widget::Update(const float Tick)
{
	if (!bIsVisible) return;

	for (auto iterator = Elements.begin(); iterator != Elements.end(); iterator++)
	{
		iterator->second->Update(Tick);
	}
}

void Widget::draw(sf::RenderTarget &Render, sf::RenderStates States) const
{
	if (!bIsVisible) return;

	for (auto iterator = Elements.begin(); iterator != Elements.end(); iterator++)
	{
		Render.draw(*iterator->second);
	}
}

#endif

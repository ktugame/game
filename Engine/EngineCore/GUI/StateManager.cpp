#include "StateManager.h"
#include "StateInGame.h"
#include "StateMainMenu.h"
#ifdef OS_LINUX
#include <unistd.h>
#endif

StateManager::StateManager(GameEngine *Engine)
{
	this->Loading = nullptr;
	this->CurrentGameState = nullptr;
	this->Engine = Engine;
	this->Loading = new LoadingView();
}

StateManager::~StateManager()
{
	if (Loading)
	{
		delete Loading;
		Loading = nullptr;
	}

	if (CurrentGameState)
	{
		delete CurrentGameState;
		CurrentGameState = nullptr;
	}
}

void StateManager::Ready()
{
	if (!Engine)
		throw "Engine is null!";

	//if (!Engine->Counter)
	//	throw  "Engine counter is null!";

	if (!this->Loading)
		throw "Manager loading is null!";

	//this->Loading->Ready(Engine->GameWindow, Engine->Counter);


	if (!Engine) return;
	CurrentGameState = new StateInGame(Engine);
}

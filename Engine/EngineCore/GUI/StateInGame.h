#pragma once

#include <SFML/Graphics.hpp>

class Camera;
class Widget;
class UIText;
class UIButton;
class GameEngine;
class World;
class BasicMap;

using uint = unsigned int;

class StateInGame
{
private:

	GameEngine *m_Engine;

	// Game camera / view port / view
	Camera *GameCamera;

	Widget *WidgetScore;
	Widget *WidgetPlayerStats;
	Widget *WidgetDebugStats;
	Widget *WidgetMenuESC;
	Widget *WidgetDeathState;

	UIText *TextFPS;
	UIText *TextScore;
	UIText *TextElapsedTime;

	UIText *TextEnemyCount;
	UIText *TextPickUpCount;
	UIText *TextSpawnerCount;

	UIText *TextPlayerHealth;
	UIText *TextPlayerStatus;

	UIButton *ButtonRestart;
	UIButton *ButtonMainMenu;
	UIButton *ButtonCloseGame;

	World *m_World;

public:

	// State constructor
	StateInGame(GameEngine *Engine);

	// State destructor
	~StateInGame();

	// Draw state
	void Draw();

	// Update state
	void Update();

};

#ifndef LOADINGVIEW_H
#define LOADINGVIEW_H
#pragma once

#include <SFML/Graphics.hpp>
#include "../Engine/FPSCounter.h"

using std::string;

// Loading view
class LoadingView
{
private:

	// FPS and Tick counter
	FPSCounter *Counter;

	// Game window
	sf::RenderWindow *GameWindow;

	// Background shape
	sf::RectangleShape Background;

	// Loading Indicator
	sf::Sprite Indicator;

	// Indicator texture
	sf::Texture IndicatorTexture;

public:

	// Loading view constructor
	LoadingView();

	// Loading view destructor
	~LoadingView();

	// Loading view update
	void Update();

	// Loading view draw
	void Draw();

	// Set status
	bool Ready(sf::RenderWindow *GameWindow, FPSCounter *Counter);

};

#endif // !LOADINGVIEW_H

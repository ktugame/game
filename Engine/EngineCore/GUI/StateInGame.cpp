#include "StateInGame.h"
#include "../Game/BasicMap.h"
#include "../Engine/GameEngine.h"
#include "../Engine/FPSCounter.h"
#include "../Engine/ContentLoader.h"
#include "../Console.h"
#include "../Engine/Settings.h"
#include "../Engine/Camera.h"

#include "StateManager.h"
#include "StateMainMenu.h"

#include "../Engine/SoundManager.h"
#include "UIManager.h"
#include "Widget.h"
#include "UIText.h"
#include "UIButton.h"
#include "UIShapeBox.h"
#include "../Game/World.h"

StateInGame::StateInGame(GameEngine *Engine)
{
	m_Engine = Engine;
	GameCamera = nullptr;

	WidgetScore = nullptr;
	WidgetPlayerStats = nullptr;
	WidgetDebugStats = nullptr;
	WidgetMenuESC = nullptr;
	WidgetDeathState = nullptr;

	TextFPS = nullptr;
	TextScore = nullptr;
	TextElapsedTime = nullptr;
	TextEnemyCount = nullptr;
	TextPickUpCount = nullptr;
	TextSpawnerCount = nullptr;
	TextPlayerHealth = nullptr;
	TextPlayerStatus = nullptr;

	ButtonRestart = nullptr;
	ButtonMainMenu = nullptr;
	ButtonCloseGame = nullptr;

	//GameCamera = new Camera(Engine->GameWindow, &Engine->MousePositionOnScreen);

	m_World = new World(Engine);

	//Engine->GameUIManager->ClearWidgets();
	//Engine->GameUIManager->Ready(GameCamera->UIViewPort);
}

StateInGame::~StateInGame()
{
	delete m_World;
	m_World = nullptr;
}

void StateInGame::Draw()
{
	//if (m_Engine->GameWindow)
	{
		if (m_World)
		{
			//m_Engine->GameWindow->draw(*m_World);
			//m_World->draw(*m_Engine->GameWindow);
		}

		//if (m_Engine->GameUIManager)
		//{
		//	m_Engine->GameWindow->draw(*m_Engine->GameUIManager);
		//}
	}
}

void StateInGame::Update()
{
//	float Tick = m_Engine->Counter->GetTick();
//
//	if (m_Engine->IsKeyDown(sf::Keyboard::Escape))
//	{
//		m_Engine->Shutdown();
//	}
//
//	if (m_Engine->GameUIManager)
//	{
//		m_Engine->GameUIManager->Update(Tick);
//	}
//
//	if (m_World)
//	{
//		m_World->Update(Tick);
//		m_World->Cleanup();
//	}
//
//	if (m_Engine->GameSoundManager)
//	{
//		m_Engine->GameSoundManager->Update();
//	}

}

#pragma once

#include "UIElements.h"

class GameEngine;

// Galima ispiesti boxa
class UIShapeBox : public UIElements
{
private:
	
	// Shape
	sf::RectangleShape Shape;

	// Engine
	GameEngine *Engine;

	// Target Window Resolution
	const sf::Vector2i *TargetWindowResolution;

public:

	UIShapeBox(GameEngine *Engine, sf::Vector2f Position, sf::Vector2f Size,
		sf::Color Color = sf::Color(25, 25, 25, 150));

	// Destruktorius
	~UIShapeBox();

	// Update box
	void Update(const float Tick);

	// Set color
	void SetColor(sf::Color Color);

	// Set size
	void SetSize(sf::Vector2f Size);

	// Set position
	void SetPosition(sf::Vector2f Position);

	// Set origin
	void SetOrigin(sf::Vector2f OriginPosition);

	// Get position
	sf::Vector2f GetPosition();

	// Get size
	sf::Vector2f GetSize();

	// Get color
	sf::Color GetColor();

protected:

	void draw(sf::RenderTarget &Render, sf::RenderStates States) const;


};

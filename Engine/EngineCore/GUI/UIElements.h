#pragma once

#include <string>
#include <SFML/Graphics.hpp>

using std::string;

// Klase skirta visiem elementams atvaizduoti, kurie yra Widget'e
class UIElements : public sf::Drawable
{
public:

	// Desturctor
	virtual ~UIElements() { }

	// Update element
	virtual void Update(const float Tick) = 0;

protected:

	// SFML draw method
	virtual void draw(sf::RenderTarget &Render, sf::RenderStates States) const = 0;

};

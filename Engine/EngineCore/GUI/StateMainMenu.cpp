#include "StateMainMenu.h"

#include "../Engine/GameEngine.h"
#include "StateManager.h"
#include "StateInGame.h"
#include "UIButton.h"

StateMainMenu::StateMainMenu(GameEngine *Engine)
{
	m_Engine = Engine;
	ButtonStart = nullptr;
	ButtonOptions = nullptr;
	ButtonAchievements = nullptr;
	ButtonAbout = nullptr;
	ButtonQuit = nullptr;


	WidgetMainMenu = nullptr;
	//WidgetOptions = nullptr;
	//WidgetAbout = nullptr;

	//Engine->GameUIManager->ClearWidgets();

	WidgetMainMenu = new Widget();
	//Engine->GameUIManager->AddWidget("mainmenu", WidgetMainMenu);


	ButtonStart = new UIButton();
	ButtonOptions = new UIButton();
	ButtonAchievements = new UIButton();
	ButtonAbout = new UIButton();
	ButtonQuit = new UIButton();

	//Function playGame = std::bind(&StateMainMenu::funtionChangeStateInGame, this);
	//Function closeGame = std::bind(&StateMainMenu::functionCloseGame, this);

	//ButtonStart->SetFunction(playGame);
	//ButtonQuit->SetFunction(closeGame);

	ButtonStart->SetText("START");
	ButtonOptions->SetText("OPTIONS");
	ButtonAchievements->SetText("ACHIEVEMENTS");
	ButtonAbout->SetText("ABOUT");
	ButtonQuit->SetText("QUIT");

	auto SizeMinus = -540 + ButtonStart->GetButtonHalfSize().y + 60;
	auto SizePlus = +540 - ButtonStart->GetButtonHalfSize().y - 60;

	ButtonStart->SetButtonPosition({ 0, SizeMinus });
	ButtonOptions->SetButtonPosition({ 0, SizeMinus * 0.5f });
	ButtonAchievements->SetButtonPosition({ 0, 0 });
	ButtonAbout->SetButtonPosition({ 0, SizePlus * 0.5f });
	ButtonQuit->SetButtonPosition({ 0, SizePlus });

	BackgroundSprite.setOrigin(BackgroundSprite.getLocalBounds().width * 0.5f,
		BackgroundSprite.getLocalBounds().height * 0.5f);
	BackgroundSprite.setPosition({ 0, 0 });

	WidgetMainMenu->AddUIElement("start", ButtonStart);
	WidgetMainMenu->AddUIElement("options", ButtonOptions);
	WidgetMainMenu->AddUIElement("achievements", ButtonAchievements);
	WidgetMainMenu->AddUIElement("about", ButtonAbout);
	WidgetMainMenu->AddUIElement("quit", ButtonQuit);
}

StateMainMenu::~StateMainMenu()
{

}

void StateMainMenu::Draw()
{
	//if (m_Engine->GameWindow)
	{
		sf::RectangleShape shape;
		shape.setFillColor({ 25,25,25 });
		shape.setPosition({ 0,0 });
		shape.setSize({ 1920, 1080 });
		shape.setOrigin(
			shape.getLocalBounds().width  * 0.5f,
			shape.getLocalBounds().height  * 0.5f
		);

		//m_Engine->GameWindow->draw(shape);

		if (WidgetMainMenu)
		{
			//m_Engine->GameWindow->draw(*WidgetMainMenu);
		}

		//if (GameUIManager)
		//{
		//	GameWindow->draw(*GameUIManager);
		//}
	}
}

void StateMainMenu::Update()
{
	//float Tick = m_Engine->Counter->GetTick();

	//auto view = m_Engine->GameWindow->getView();

	//view.setCenter({ 0, 0 });

	//m_Engine->GameWindow->setView(view);

	BackgroundSprite.setPosition(0, 0);

	//if (GameUIManager)
	//{
	//	GameUIManager->Update(Tick);
	//}

	if (WidgetMainMenu)
	{
		//WidgetMainMenu->Update(Tick);
	}
}

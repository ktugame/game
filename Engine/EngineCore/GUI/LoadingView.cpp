#include "LoadingView.h"

#ifdef OS_WINDOWS
#include "resource.h"
#include <Windows.h>
#endif

LoadingView::LoadingView()
{
	this->Background.setFillColor({ 25, 25, 25 });
	this->Background.setPosition({ 0, 0 });
}

LoadingView::~LoadingView()
{
	GameWindow = nullptr;
}

void LoadingView::Update()
{
	sf::View View = GameWindow->getView();
	View.setCenter({ 0,0 });
	GameWindow->setView(View);

	if (GameWindow)
	{
		sf::Vector2u WinSize = GameWindow->getSize();
		float WinW = static_cast<float>(WinSize.x), WinH = static_cast<float>(WinSize.y);
		Background.setSize({ WinW, WinH });
		Background.setOrigin(Background.getLocalBounds().width * 0.5f, Background.getLocalBounds().height * 0.5f);
	}

	Indicator.rotate(42.0f * Counter->GetTick());
}

void LoadingView::Draw()
{
	if (GameWindow)
	{
		GameWindow->draw(Background);
		GameWindow->draw(Indicator);
	}
}

bool LoadingView::Ready(sf::RenderWindow *GameWindow, FPSCounter *Counter)
{
	this->GameWindow = GameWindow;
	this->Counter = Counter;

	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms648045(v=vs.85).aspx
	//HANDLE img = LoadImage(hInstance, MAKEINTRESOURCE(IDB_PNG1), 0, 0, 0, 0x00002000);
	//HBITMAP bitmap = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_PNG1));
	//GetHandleInformation(img, 0);
	//IndicatorTexture.loadFromStream(	bitmap	);
	//IndicatorTexture.loadFromFile(		bitmap	);
	//IndicatorTexture.loadFromMemory(	bitmap,	);
	//IndicatorTexture.loadFromStream(	bitmap	);

	{
#ifdef OS_WINDOWS
		char buffer[MAX_PATH];
		GetModuleFileName(NULL, buffer, MAX_PATH);
		string Path = buffer;
		Path = Path.substr(0, Path.find_last_of("/\\"));

		bool result = IndicatorTexture.loadFromFile(Path + "\\Graphics\\LoadIndicator.png");

		if (!result)
		{
			// TODO: change this to our standart
			MessageBox(NULL, "Failed to find load indicator\nLoading view", "Engien error", MB_OK | MB_ICONERROR);
		}
#elif OS_LINUX

		IndicatorTexture.loadFromFile("Graphics/LoadIndicator.png");
#endif

		Indicator.setTexture(IndicatorTexture);
		Indicator.setOrigin(Indicator.getLocalBounds().width * 0.5f, Indicator.getLocalBounds().height * 0.5f);
		Indicator.setPosition({ 0,0 });
		Background.setPosition({ 0,0 });

	}

	return true;
}

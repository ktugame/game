#include "UIShapeBox.h"
#include "../Engine/GameEngine.h"

UIShapeBox::UIShapeBox(GameEngine *Engine, sf::Vector2f Position, sf::Vector2f Size, sf::Color Color)
{
	this->Engine = Engine;
	this->TargetWindowResolution = &Engine->TargetWindowResolution;

	Shape.setFillColor(Color);

	SetSize(Size);
	SetPosition(Position);
}

UIShapeBox::~UIShapeBox()
{

}

void UIShapeBox::draw(sf::RenderTarget &Render, sf::RenderStates States) const
{
	Render.draw(Shape, States);
}

void UIShapeBox::Update(const float Tick)
{

}

void UIShapeBox::SetColor(sf::Color Color)
{
	Shape.setFillColor(Color);
}

void UIShapeBox::SetSize(sf::Vector2f Size)
{
	Shape.setSize(Size);
	Shape.setOrigin(Size * 0.5f);
}

void UIShapeBox::SetPosition(sf::Vector2f Position)
{
	if (!Engine || !TargetWindowResolution) return;

	auto View = sf::View().getSize(); // Engine->GameWindow->getDefaultView().getSize();
	auto Rez = Engine->TargetWindowResolution;

	float xRatio = View.x / TargetWindowResolution->x;
	float yRatio = View.y / TargetWindowResolution->y;

	Position.x *= xRatio;
	Position.y *= yRatio;

	Shape.setPosition(Position);
}

void UIShapeBox::SetOrigin(sf::Vector2f OriginPosition)
{
	Shape.setOrigin(OriginPosition);
}

sf::Vector2f UIShapeBox::GetPosition()
{
	return Shape.getPosition();
}

sf::Vector2f UIShapeBox::GetSize()
{
	return Shape.getSize();
}

sf::Color UIShapeBox::GetColor()
{
	return Shape.getFillColor();
}

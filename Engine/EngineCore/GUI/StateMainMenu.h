#pragma once

#include "../Engine/Camera.h"
#include "UIManager.h"
#include "../Engine/ContentLoader.h"
#include "../Console.h"
#include "../Object/PickUp.h"
#include "UIText.h"
#include "UIButton.h"
#include "Widget.h"
#include "../Engine/FPSCounter.h"
#include "../Engine/GameEngine.h"

class UIButton;
class UIText;
class Widget;
class Elements;
class GameEngine;

class StateMainMenu
{
private:

	GameEngine *m_Engine;

	sf::Texture Texture;

	sf::Sprite BackgroundSprite;

	UIButton
		*ButtonStart,
		*ButtonOptions,
		*ButtonAchievements,
		*ButtonAbout,
		*ButtonQuit;

	Widget *WidgetMainMenu;
	//Widget *WidgetOptions;
	//Widget *WidgetAbout;

public:

	// State constructor
	StateMainMenu(GameEngine *Engine);

	// State destructor
	~StateMainMenu();

	// Draw state
	void Draw();

	// Update state
	void Update();

private:

};

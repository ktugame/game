#include "UIButton.h"
#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "../Engine/DEBUG.h"
#include "../Engine/Settings.h"
#include <SFML/Graphics.hpp>

UIButton::UIButton()
{
	TargetWindowResolution = nullptr;//&Engine->TargetWindowResolution;

	ButtonFunction = nullptr;

	// Half size of the button
	HalfSize = { 275, 70 };

	Text.setString("None");
	TextShadow.setString("None");

	Text.setCharacterSize(80);
	TextShadow.setCharacterSize(80);

	//Text.setFont(*Engine->GameContent->GetFonts()["OswaldRegular"]);
	//TextShadow.setFont(*Engine->GameContent->GetFonts()["OswaldRegular"]);

	Text.setOrigin({
		Text.getGlobalBounds().width * 0.5f,
		Text.getGlobalBounds().height * 0.5f
	});
	TextShadow.setOrigin({
		TextShadow.getGlobalBounds().width * 0.5f,
		TextShadow.getGlobalBounds().height * 0.5f
	});

	//Text.setFillColor({ 242, 242,242 });
	//TextShadow.setFillColor({ 0, 0, 0 });

	Text.setPosition({ 0,0 });
	TextShadow.setPosition({ 0,0 });

	ButtonBackground.setFillColor(DefaultColor);
	ButtonBackground.setSize({ HalfSize.x * 2, HalfSize.y * 2 });
	ButtonBackground.setPosition({ 0,0 });
	ButtonBackground.setOrigin(HalfSize);
	Offset = sf::Vector2f({ -160,-15 });
	Position = sf::Vector2f({ 0,0 });

	VertexArr.setPrimitiveType(sf::Quads);
	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));
	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));
	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));
	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));

	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));
	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));
	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));
	VertexArr.append(sf::Vertex( sf::Vector2f(0,0) ));

	for (uint i = 0; i < VertexArr.getVertexCount(); i++)
		VertexArr[i].color = sf::Color(100, 100, 100);
}

UIButton::~UIButton()
{
	this->ButtonFunction = nullptr;
	this->TargetWindowResolution = nullptr;
}

void UIButton::Update(const float Tick)
{
	if (!ButtonFunction)
	{
		return;
	}

	float Delta = Tick * 1000.0f;

	auto View = sf::View().getSize();//Engine->GameWindow->getDefaultView().getSize();

	sf::Vector2f MousePosition =
		sf::Vector2f(
			Engine->MousePositionOnScreen.x +
			static_cast<float>(View.x) * -0.5f,
			Engine->MousePositionOnScreen.y +
			static_cast<float>(View.y) * -0.5f);

	if (ButtonBackground.getGlobalBounds().contains(MousePosition))
	{
		float XPosition = VertexArr[6].position.x - 47;

		if (VertexArr[4].position.x < XPosition)
		{
			VertexArr[4].position.x += Delta;
			VertexArr[5].position.x += Delta;
		}
		else
		{
			VertexArr[4].position.x = XPosition;
			VertexArr[5].position.x = XPosition;
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			if (ButtonFunction)
			{
				ButtonFunction();
				return;
			}
		}
	}
	else
	{
		if (VertexArr[4].position.x > VertexArr[0].position.x + 90)
		{
			VertexArr[4].position.x -= Delta;
			VertexArr[5].position.x -= Delta;
		}
		else
		{
			VertexArr[4].position = Position + sf::Vector2f(-HalfSize.x + 90, -HalfSize.y);
			VertexArr[5].position = Position + sf::Vector2f(-HalfSize.x + 90, +HalfSize.y);
		}

		for (uint i = 0; i < VertexArr.getVertexCount(); i++)
		{
			VertexArr[i].color = sf::Color(255, 147, 30);
		}
	}
}

bool UIButton::ContainsPoint(sf::Vector2f Point)
{
	return ButtonBackground.getGlobalBounds().contains(Point);
}

void UIButton::SetTextSize(unsigned int Size)
{
	//this->Text.setCharacterSize(Size);
	//this->Text.setOrigin({
	//	Text.getGlobalBounds().width * 0.5f,
	//	Text.getGlobalBounds().height * 0.5f
	//});
}

void UIButton::SetButtonSize(sf::Vector2f Size)
{
	this->ButtonBackground.setSize(Size);
}

void UIButton::SetTextOffsetPosition(sf::Vector2f Position)
{
	this->Offset = Position;
}

void UIButton::SetButtonPosition(sf::Vector2f Position)
{
	if (!TargetWindowResolution) return;

	auto View = sf::View().getSize(); // Engine->GameWindow->getDefaultView().getSize();
	auto Rez = Engine->TargetWindowResolution;

	float xRatio = View.x / TargetWindowResolution->x;
	float yRatio = View.y / TargetWindowResolution->y;

	Position.x *= xRatio;
	Position.y *= yRatio;

	this->Position = Position;
	this->ButtonBackground.setPosition(Position);

	Text.setPosition(
		Position + Offset);
	TextShadow.setPosition(
		Position + Offset
		+ sf::Vector2f(4, 5)
	);

	//this->VertexArr[0].position = Position + sf::Vector2f(-275, -70);
	//this->VertexArr[1].position = Position + sf::Vector2f(-275, +70);
	//this->VertexArr[2].position = Position + sf::Vector2f(+275, +70);
	//this->VertexArr[3].position = Position + sf::Vector2f(+275, -70);

	this->VertexArr[0].position = Position + sf::Vector2f(-HalfSize.x, -HalfSize.y);
	this->VertexArr[1].position = Position + sf::Vector2f(-HalfSize.x, +HalfSize.y);
	this->VertexArr[2].position = Position + sf::Vector2f(-HalfSize.x + 47, +HalfSize.y);
	this->VertexArr[3].position = Position + sf::Vector2f(-HalfSize.x + 47, -HalfSize.y);

	this->VertexArr[4].position = Position + sf::Vector2f(-HalfSize.x + 90, -HalfSize.y);
	this->VertexArr[5].position = Position + sf::Vector2f(-HalfSize.x + 90, +HalfSize.y);
	this->VertexArr[6].position = Position + sf::Vector2f(+HalfSize.x, +HalfSize.y);
	this->VertexArr[7].position = Position + sf::Vector2f(+HalfSize.x, -HalfSize.y);

}

sf::Vector2f UIButton::GetButtonHalfSize()
{
	return HalfSize;
}

void UIButton::SetFunction(Function CallFunction)
{
	this->ButtonFunction = CallFunction;

	for (uint i = 0; i < VertexArr.getVertexCount(); i++)
		VertexArr[i].color = sf::Color(255, 147, 30);
}

void UIButton::SetText(std::string Text)
{
	this->Text.setString(Text);
	this->TextShadow.setString(Text);
}

void UIButton::draw(sf::RenderTarget &Render, sf::RenderStates States) const
{
	//Render.draw(ButtonBackground, States);
	Render.draw(VertexArr, States);
	Render.draw(TextShadow, States);
	Render.draw(Text, States);
}

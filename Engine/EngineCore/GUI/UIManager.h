#pragma once

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <map>

class Widget;

typedef const char * WName;

using std::map;

// User inferce manager
class UIManager : public sf::Drawable
{
private:

	// Pointing to the pointer
	sf::View *UIView;

	// Widget sarasas
	map<WName, Widget*> Widgets;

public:

	// Constructor
	UIManager();

	// Destructor
	~UIManager();

	// Prideda nauja widget'a
	void AddWidget(WName Name, Widget *widget);

	// Give ready status
	// This function calls other functions:
	// * ChangeUIView
	void Ready(sf::View *UIView);

	// Update widgets
	void Update(const float Tick);

	// Remove all widget
	void ClearWidgets();

protected:

	void draw(sf::RenderTarget &Render, sf::RenderStates States) const;

};

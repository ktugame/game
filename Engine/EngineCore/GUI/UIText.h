#pragma once

#include "UIElements.h"
#include <string>

class GameEngine;

using std::string;

// Klase skirta atvaizduoti tekstas
class UIText 
	: public UIElements
{
private:

	// Text of the button
	sf::Text Text;

	// Target Window Resolution
	const sf::Vector2i *TargetWindowResolution;

	GameEngine *Engine;

public:

	// Constructor
	// Engine pointer
	// Text
	// Position
	UIText(GameEngine *Engine, string Text, sf::Vector2f Position);

	// Destrctor
	~UIText();

	// Update element
	void Update(const float Tick);

	// Set text
	void SetText(string Text);

	// Set font size
	void SetFontSize(unsigned int FontSize);

	// Set font position on the screen
	void SetPosition(sf::Vector2f Position);

	// Set font
	void SetFont(const sf::Font *Font);

	// Set font color
	void SetColor(sf::Color Color);

protected:

	// SFML DRAW METHOD
	void draw(sf::RenderTarget &Render, sf::RenderStates States) const;

};

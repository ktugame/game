#pragma once

#ifndef TESTING
#include <SFML/Graphics/Drawable.hpp>
#endif
#include <map>

class UIElements;
class UIButton;
class UIText;
class UIShapeBox;

typedef const char* UIName;

using std::map;

// Klase skirta sukurti tam tikrus "meniu", ar paprastus "Overlay"
class Widget
#ifndef TESTING 
	: public sf::Drawable
#endif
{
private:

#ifndef TESTING

	// Elements
	map<UIName, UIElements*> Elements;

#endif

	// Destroy state
	bool bDestroy;

	// Visibility
	bool bIsVisible;

public:

	// Contructor
	// Engine - Game engine pointer (default this or Engine)
	Widget();

	// Destruktorius
	~Widget();

	// Object state
	bool Destroy();

	// Destoy widget, remove all elements from the memory
	void DestoyWidget();

#ifndef TESTING

	// Update the widgets
	void Update(const float Tick);

	// Add ui element
	void AddUIElement(UIName Name, UIElements *element);

	// Clear all elements
	void ClearUIElements();

	// Get ui element by name
	UIElements *GetUIElement(UIName Name);

#endif // !TESTING

	// Show or no
	void ToggleVisibility();

	// Hide widget
	void Hide();

	// Show widget
	void Show();

	// Is widget visible?
	bool IsVisible();

protected:

#ifndef TESTING

	void draw(sf::RenderTarget &Render, sf::RenderStates States) const;

#endif

};

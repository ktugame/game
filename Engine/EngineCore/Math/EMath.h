#ifndef EMATH_H
#define EMATH_H
#pragma once

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <SFML/System/Vector2.hpp>
#include <math.h>
#include <random>

using int8 = char;
using uint8 = unsigned char;
using int16 = short;
using uint16 = unsigned short;
using int32 = int;
using uint32 = unsigned int;
using int64 = long long;
using uint64 = unsigned long long;

constexpr float CONST_RADIANS_TO_DEG = 180.0f / static_cast<float>(M_PI);
constexpr float CONST_DEG_TO_RADIANS = static_cast<float>(M_PI) / 180.0f;
constexpr float CONST_1_DIV_SQRT_2 = 0.707106781f; // 1/sqrt(2)

namespace Math
{
	// Get length of the vector
	float GetLength(const sf::Vector2f &vec);

	// Get distance between vectors
	float Distance(const sf::Vector2f &vec1, const sf::Vector2f &vec2);

	// "normalizuotas" vektorius
	sf::Vector2f Normalize(const sf::Vector2f &vec);

	// Returns random integer from Min to Max
	// Defaults: Min = 0 Max = 10
	int Random(const int Min = 0, const int Max = 10);

	// Returns random float from Min to Max
	// Defaults: Min = 0.0f Max = 10.0f
	float Random(const float Min = 0.0f, const float Max = 10.0f);

	// Returns random coordinates from Min to Max
	// Default: Min = -5.0f, -5.0f 
	// Default: Max =  5.0f,  5.0f
	sf::Vector2f Random(
		const sf::Vector2f Min = sf::Vector2f(-5.0f, -5.0f),
		const sf::Vector2f Max = sf::Vector2f(5.0f, 5.0f));

}

#endif // !EMATH_H

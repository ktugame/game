#include "EMath.h"

namespace Math
{
	float GetLength(const sf::Vector2f &vec)
	{
		return sqrtf((vec.x * vec.x) + (vec.y * vec.y));
	}

	float Distance(const sf::Vector2f &vec1, const sf::Vector2f &vec2)
	{
		return GetLength(sf::Vector2f(vec2 - vec1));
	}

	sf::Vector2f Normalize(const sf::Vector2f &vec)
	{
		float Length = GetLength(vec);

		if (Length != 0)
		{
			return sf::Vector2f(vec.x / Length, vec.y / Length);
		}
		else
		{
			return vec;
		}
	}

	int Random(const int Min, const int Max)
	{
		return
			Min <= Max ?
			rand() % (Max - Min + 1) + Min :
			rand() % (Min - Max + 1) + Max;
	}

	float Random(const float Min, const float Max)
	{
		float random = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
		float range = Max - Min;
		return (random * range) + Min;
	}

	sf::Vector2f Random(const sf::Vector2f Min, const sf::Vector2f Max)
	{
		float x = Random(Min.x, Max.x);
		float y = Random(Min.y, Max.y);
		return sf::Vector2f(x, y);
	}

}

#pragma once

#include <SFML/Graphics.hpp>

class Player;
class SpawnersSpawner;
class PickUpSpawner;
class GameEngine;
class BasicMap;

class World : public sf::Drawable
{
private:

	// Game engine
	GameEngine *m_Engine;

	// Player
	Player *m_Player;

	// Enemy spawners spawner
	SpawnersSpawner *m_Spawner;

	// Pickup spawner
	PickUpSpawner *m_PickupSpawner;

	// Map object
	BasicMap *m_Map;

public:

	// Constructor
	World(GameEngine *Engine);

	// Destructor
	~World();

	// Update
	void Update(const float Tick);

	// Cleanup
	void Cleanup();

	// draw
	//void draw(sf::RenderTarget &Render);


private:

	// SFML draw method
	virtual void draw(sf::RenderTarget &Render, sf::RenderStates States) const;

};

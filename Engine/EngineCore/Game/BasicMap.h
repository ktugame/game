#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class BasicMap //: public sf::Drawable
{
private:

	int m_mapSize;

	float m_tileSize;

	sf::Sprite m_sprite;

public:

	BasicMap(sf::Texture *Texture);

	~BasicMap();

	void Draw(sf::RenderWindow *Window);

private:


};

#include "BasicMap.h"

BasicMap::BasicMap(sf::Texture *Texture)
{
	m_mapSize = 40;
	m_tileSize = 512.0;

	if (!Texture)
		throw "Texture is null!";

	m_sprite.setTexture(*Texture);
	m_sprite.setScale(1.0005f, 1.0005f);
}

BasicMap::~BasicMap()
{

}

void BasicMap::Draw(sf::RenderWindow *Window)
{
	for (int y = 0; y < m_mapSize; y++)
	{
		for (int x = 0; x < m_mapSize; x++)
		{
			if ((x + y) % 4 == 0)
			{
				m_sprite.setTextureRect(sf::IntRect(
					0, 0,
					static_cast<int>(m_tileSize), static_cast<int>(m_tileSize)
				));
			}
			else if ((x + y) % 3 == 0)
			{
				m_sprite.setTextureRect(sf::IntRect(
					static_cast<int>(m_tileSize), 0,
					static_cast<int>(m_tileSize), static_cast<int>(m_tileSize)
				));
			}
			else if ((x + y) % 2 == 0)
			{
				m_sprite.setTextureRect(sf::IntRect(
					0, static_cast<int>(m_tileSize),
					static_cast<int>(m_tileSize), static_cast<int>(m_tileSize)
				));
			}
			else
			{
				m_sprite.setTextureRect(sf::IntRect(
					static_cast<int>(m_tileSize), static_cast<int>(m_tileSize),
					static_cast<int>(m_tileSize), static_cast<int>(m_tileSize)
				));
			}

			m_sprite.setPosition(
				static_cast<float>(x) * m_tileSize - m_tileSize * 10.0f,
				static_cast<float>(y) * m_tileSize - m_tileSize * 10.0f
			);

			Window->draw(m_sprite);
		}
	}
}

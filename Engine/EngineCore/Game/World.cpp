#include "World.h"

#include "../Engine/GameEngine.h"
#include "../Engine/ContentLoader.h"
#include "../Math/EMath.h"

#include "../Object/SpawnersSpawner.h"
#include "../Object/PickUpSpawner.h"
#include "../Object/EnemySpawner.h"

#include "../Object/Wall.h"
#include "../Object/Blood.h"
#include "BasicMap.h"
#include "../Object/PWeapon.h"
#include "../Object/ParticleSystem.h"

#include "../Object/WPistol1.h"
#include "../Object/WMossberg590.h"

#include "../Object/Player.h"
#include "../Object/Enemy.h"

World::World(GameEngine *Engine)
{
	m_Engine = Engine;

	//m_Player = new Player(Engine, { 0, 0 });
	//m_Player->GivePlayerWeapon(new WPistol1(Engine));

	m_Spawner = new SpawnersSpawner(Engine, &m_Player);
	m_PickupSpawner = new PickUpSpawner(Engine, &m_Player);
	//m_Map = new BasicMap(Engine->GameContent->GetTextures()["MapBackground"]);

	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * 3, 64 * 3)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * 3, 64 * 4)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * 4, 64 * 3)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * 5, 64 * 1)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * -3, 64 * 3)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * 4, 64 * -4)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * 5, 64 * -5)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * -5, 64 * 5)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * -3, 64 * 4)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * -5, 64 * -5)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * -4, 64 * -5)));
	Wall::StaticWallList.push_back(Wall(Engine, sf::Vector2f(64 * -3, 64 * -4)));

}

World::~World()
{
	//DELETE_OBJ(m_Player);
	//DELETE_OBJ(m_Spawner);
	//DELETE_OBJ(m_PickupSpawner);
	//DELETE_OBJ(m_Map);

	for (uint i = 0; i < PickUp::StaticPickupList.size(); i++)
	{
		delete PickUp::StaticPickupList[i];
		PickUp::StaticPickupList[i] = nullptr;
	}

	for (uint i = 0; i < EnemySpawner::StaticSpawnerList.size(); i++)
	{
		delete EnemySpawner::StaticSpawnerList[i];
		EnemySpawner::StaticSpawnerList[i] = nullptr;
	}

	VBullet::StaticBulletList.clear();
	Wall::StaticWallList.clear();
	Blood::StaticBloodList.clear();
}

void World::Update(const float Tick)
{
//	if (m_PickupSpawner) m_PickupSpawner->Update(Tick);
//	if (m_Spawner) m_Spawner->Update(Tick);
//
//	for (uint32 i = 0; i < VBullet::StaticBulletList.size(); i++)
//		VBullet::StaticBulletList[i].Update(Tick);
//
//	for (uint32 i = 0; i < Wall::StaticWallList.size(); i++)
//		Wall::StaticWallList[i].Update(Tick);
//
//	for (uint32 i = 0; i < Blood::StaticBloodList.size(); i++)
//		Blood::StaticBloodList[i].Update(Tick);
//
//	for (uint32 i = 0; i < PickUp::StaticPickupList.size(); i++)
//		if (PickUp::StaticPickupList[i])
//			PickUp::StaticPickupList[i]->Update(Tick);
//
//	if (m_Player) m_Player->Update(Tick);
//
//	for (uint32 i = 0; i < Enemy::StaticEnemyList.size(); i++)
//		if (Enemy::StaticEnemyList[i])
//			Enemy::StaticEnemyList[i]->Update(Tick);
//
//	for (uint32 i = 0; i < EnemySpawner::StaticSpawnerList.size(); i++)
//		EnemySpawner::StaticSpawnerList[i]->Update(Tick);
//
//	for (uint32 i = 0; i < ParticleSystem::StaticParticleList.size(); i++)
//		ParticleSystem::StaticParticleList[i]->Update(Tick);
//
//	// TODO: Perkurti sistema
//	// OLD: Kazkodel crashinasi nuolatos
//	for (uint32 k = 0; k < VBullet::StaticBulletList.size(); k++)
//	{
//		for (uint32 i = 0; i < Enemy::StaticEnemyList.size(); i++)
//		{
//			// TODO: optimizations needed
//
//			// NOTE:
//			// DEL SIO IF'o crashai galejo dingti!
//			if (VBullet::StaticBulletList.size() > 0 && Enemy::StaticEnemyList.size() > 0)
//			{
//				if (Enemy::StaticEnemyList[i]->InRange(VBullet::StaticBulletList[k].GetPosition()))
//				{
//					Enemy::StaticEnemyList[i]->TakeDamage(
//						VBullet::StaticBulletList[k].GetDamage());
//					VBullet::StaticBulletList.erase(
//						VBullet::StaticBulletList.begin() + k);
//					Blood::StaticBloodList.push_back(
//						Blood(m_Engine, Enemy::StaticEnemyList[i]->GetPosition()));
//					i = k = 0;
//				}
//			}
//		}
//		for (uint32 j = 0; j < EnemySpawner::StaticSpawnerList.size(); j++)
//		{
//			if (VBullet::StaticBulletList.size() > 0 && EnemySpawner::StaticSpawnerList.size() > 0)
//			{
//				if (EnemySpawner::StaticSpawnerList[j]->InRange(VBullet::StaticBulletList[k].GetPosition()))
//				{
//					EnemySpawner::StaticSpawnerList[j]->TakeDamage(VBullet::StaticBulletList[k].GetDamage());
//					VBullet::StaticBulletList.erase(VBullet::StaticBulletList.begin() + k);
//					k = 0;
//				}
//			}
//		}
//
//	}
//
//	if (m_Player)
//	{
//		for (uint32 i = 0; i < Enemy::StaticEnemyList.size(); i++)
//		{
//			auto EnemyPointer = Enemy::StaticEnemyList[i];
//
//			float DistPlayer = Math::Distance(
//				m_Player->GetPosition(), EnemyPointer->GetPosition());
//
//			if (DistPlayer < 55.0f)
//			{
//
//				// TODO: Fixes
//				// Kai cia deletinam playeri - visur viskas crashinasi
//				// Nes per daug visur perduodam pointerius
//				// Reik sugalvoti sistema, kaip nesicrashintu
//
//				//delete player;
//				//player = nullptr;
//
//				// This works perfectly
//				//player->TakeDamage(Enemy::StaticEnemyList[i]->DoDamage());
//				m_Player->TakeDamage(EnemyPointer->Attack());
//
//				if (!m_Player->IsAlive())
//				{
//					//WidgetScore->Show();
//					//WidgetPlayerStats->Hide();
//					//
//					//if (TextPlayerStatus)
//					//	TextPlayerStatus->SetText("STATUS - DEAD");
//
//					delete m_Player;
//					m_Player = nullptr;
//				}
//
//				delete EnemyPointer;
//				Enemy::StaticEnemyList.erase(Enemy::StaticEnemyList.begin() + i);
//
//				i = (i - 1 < 0 ? 0 : i--);
//
//				if (!m_Player)
//					break;
//			}
//		}
//	}
//
//	if (m_Player)
//	{
//		for (uint32 i = 0; i < PickUp::StaticPickupList.size(); i++)
//		{
//			float DistPlayer = Math::Distance(
//				m_Player->GetPosition(), PickUp::StaticPickupList[i]->GetPosition());
//
//			if (DistPlayer <= 40.0f)
//			{
//				//m_Player->AddPickupStatus(PickUp::StaticPickupList[i]);
//			}
//		}
//
//	}
}

void World::Cleanup()
{
	// ------------------------------------------------- Clean up

	for (uint32 i = 0; i < VBullet::StaticBulletList.size(); i++)
	{
		if (VBullet::StaticBulletList[i].DestroyState())
		{
			VBullet::StaticBulletList.erase(VBullet::StaticBulletList.begin() + i);
			i = i > 0 ? i - 1 : 0;
		}
	}
	for (uint32 i = 0; i < Enemy::StaticEnemyList.size(); i++)
	{
		if (!Enemy::StaticEnemyList[i]->IsAlive())
		{
			delete Enemy::StaticEnemyList[i];
			Enemy::StaticEnemyList.erase(Enemy::StaticEnemyList.begin() + i);
			i = i > 0 ? i - 1 : 0;
		}
	}
	for (uint32 i = 0; i < EnemySpawner::StaticSpawnerList.size(); i++)
	{
		if (EnemySpawner::StaticSpawnerList[i]->DestroyState())
		{
			delete EnemySpawner::StaticSpawnerList[i];
			EnemySpawner::StaticSpawnerList.erase(EnemySpawner::StaticSpawnerList.begin() + i);
			//m_Spawner->DeathCount++;
			i = i > 0 ? i - 1 : 0;
		}
	}
	for (uint32 i = 0; i < PickUp::StaticPickupList.size(); i++)
	{
		if (PickUp::StaticPickupList[i]->DestroyState())
		{
			delete PickUp::StaticPickupList[i];
			PickUp::StaticPickupList.erase(PickUp::StaticPickupList.begin() + i);
			i = i > 0 ? i - 1 : 0;
		}
	}

	for (uint32 i = 0; i < ParticleSystem::StaticParticleList.size(); i++)
	{
		if (ParticleSystem::StaticParticleList[i]->IsDone())
		{
			delete ParticleSystem::StaticParticleList[i];
			ParticleSystem::StaticParticleList.erase(
				ParticleSystem::StaticParticleList.begin() + i);
			i = i > 0 ? i - 1 : 0;
		}
	}

	for (uint32 i = 0; i < Blood::StaticBloodList.size(); i++)
	{
		if (Blood::StaticBloodList[i].DestroyState())
		{
			Blood::StaticBloodList.erase(Blood::StaticBloodList.begin() + i);
			i = i > 0 ? i - 1 : 0;
		}
	}
}

void World::draw(sf::RenderTarget &Render, sf::RenderStates States) const
{
	// Draw map
	//if (m_Map) m_Map->Draw(m_Engine->GameWindow);

	// Drawing pickup spawner
	//if (m_PickupSpawner) Render.draw(*m_PickupSpawner);

	// Drawins spawners spawner
	//if (m_Spawner) Render.draw(*m_Spawner);

	for (uint32 i = 0; i < EnemySpawner::StaticSpawnerList.size(); i++)
		Render.draw(*EnemySpawner::StaticSpawnerList[i]);

	// Drawing walls
	for (uint32 i = 0; i < Wall::StaticWallList.size(); i++)
		Render.draw(Wall::StaticWallList[i]);

	// Drawing pickups
	for (uint32 i = 0; i < PickUp::StaticPickupList.size(); i++)
		Render.draw(*PickUp::StaticPickupList[i]);

	// Drawing blood
	for (uint32 i = 0; i < Blood::StaticBloodList.size(); i++)
		Render.draw(Blood::StaticBloodList[i]);

	// Drawing enemies (actors)
	for (uint32 i = 0; i < EnemySpawner::StaticSpawnerList.size(); i++)
	{
		for (uint32 j = 0; j < Enemy::StaticEnemyList.size(); j++)
		{
			//Engine->Mutex.lock();
			if (Enemy::StaticEnemyList[j])
				Render.draw(*Enemy::StaticEnemyList[j]);
			//Engine->Mutex.unlock();
		}
	}

	// Draw bullets
	for (uint32 i = 0; i < VBullet::StaticBulletList.size(); i++)
		Render.draw(VBullet::StaticBulletList[i]);

	// Draw player and gun (gun moved into player's draw)
	//if (m_Player)
	//	Render.draw(*m_Player);

	for (uint32 i = 0; i < ParticleSystem::StaticParticleList.size(); i++)
	{
		//Engine->Mutex.lock();
		if (ParticleSystem::StaticParticleList[i])
			Render.draw(*ParticleSystem::StaticParticleList[i]);
		//Engine->Mutex.unlock();
	}
}
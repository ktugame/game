#include <iostream>
#include <functional>
#include <map>
#include <set>

typedef std::function<void()> Func;
typedef std::function<void()> *FuncPointer;

class ClassA
{
public:
	Func F1, F2;

	ClassA()
	{
		F1 = std::bind(&ClassA::PrintA, this);
	}

	void LaunchFunction1()
	{
		F1();
	}

	void LaunchFunction2()
	{
		F2();
	}

private:
	void PrintA()
	{
		std::cout << "WORKS A" << std::endl;
	}
};

class ClassB
{
public:
	Func F1, F2;

	ClassB()
	{
		F1 = std::bind(&ClassB::PrintB, this);
	}

	void LaunchFunction1()
	{
		F1();
	}

	void LaunchFunction2()
	{
		F2();
	}

	void Bind(Func f)
	{
		F2 = f;
	}

private:
	void PrintB()
	{
		std::cout << "WORKS B" << std::endl;
	}
};

void FUNC1()
{
	std::cout << "BASIC FUNC1 " << std::endl;
}

void FUNC2()
{
	std::cout << "BASIC FUNC2 " << std::endl;
}

int main()
{
	std::cout << "# >> Classes function" << std::endl;

	{
		ClassA *a = new ClassA();
		ClassB *b = new ClassB();

		a->LaunchFunction1();
		b->LaunchFunction1();

		Func q = std::bind(&ClassA::LaunchFunction1 , a);
		b->Bind(q);

		b->LaunchFunction2();

		delete a;
		delete b;
		a = nullptr;
		b = nullptr;
	}

	std::cout << "# >> Basic function" << std::endl;
	  
	{
		Func f1 = FUNC1;
		Func f2 = FUNC2;
	
		f1();
		f2();
	}

	_getwch();
	return 0;
}

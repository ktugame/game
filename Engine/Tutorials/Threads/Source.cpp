#include <thread>
#include <iostream>
#include <mutex>
#include <Windows.h>

using std::thread;
using std::mutex;
using std::cout;
using std::endl;

mutex Mutex;

bool threads[2] = { false, false };

void function(int id)
{
	while (1)
	{
		cout << "Thread " << std::this_thread::get_id() << "\t C_ID: " << id << endl;
		Sleep(500);
		if (threads[id - 1]) break;
	}
}

int main()
{
	thread *th1 = nullptr, *th2 = nullptr;

	th1 = new thread(function, 1), Sleep(50), th2 = new thread(function, 2);

	while (1)
	{
		char val = _getwch();

		if (val == 'q') break;

		if (val == '1')
		{
			if (th1)
				cout << th1->get_id() << endl;
		}
		else if (val == '2')
		{
			if (th2)
				cout << th2->get_id() << endl;
		}
		else if (val == 'a')
		{
			if (!th1) continue;

			cout << "Cleaning up" << endl;
			threads[0] = true;

			th1->detach();
			delete th1;
			th1 = nullptr;
		}
		else if (val == 's')
		{
			if (!th2) continue;

			cout << "Cleaning up" << endl;
			threads[1] = true;

			th2->join();
			delete th2;
			th2 = nullptr;
		}
		else if (val == 'w')
		{
			threads[0] = threads[1] = false;
			th1 = new thread(function, 1), Sleep(50), th2 = new thread(function, 2);
		}
	}
	cout << "Done, waiting for any key..." << endl;
	_getwch();
	return 0;
}

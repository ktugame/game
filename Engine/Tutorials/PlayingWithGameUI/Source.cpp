#include <Windows.h>
#include <map>
#include <set>
#include <iostream>
#include <vector>
#include <string>

// You can use String or string its the same
typedef std::string string;
#define String string

// Typedefines
typedef unsigned int uint32;
typedef int int32;

// MSG("String" / doubleValue / floatValue ...);
#define MSG(msg) std::cout << msg << std::endl

class Element
{
protected:
	
	// Just string for checking object statuses
	String str;

public:

	// Just random variable
	// Check debugging stats how this variable changes from false to true
	bool Deleted = false;

	// Default destructor
	Element()
	{
		str = "Not set";
	}

	// Default destructor
	~Element()
	{

	}

	// Virtual function
	virtual void Print() = 0;

};

class Text : public Element
{
private:

public:

	// Default constructor
	Text() : Element()
	{
		str = "Text";
	}

	// Default destructor
	~Text()
	{

	}

	// It will be used later
	void Print()
	{
		MSG("Text: Printing");
	}

};

class Line : public Element
{
private:

public:

	// Default constructor
	Line() : Element()
	{
		str = "Line";
	}

	// Default destructor
	~Line()
	{

	}

	// It will be used later
	void Print()
	{
		MSG("Line: Printing");
	}

};

class Widget
{
private:

	// Vector of elements
	std::vector<Element *> *Elements;

public:

	// Just random variable
	// Check debugging stats how this variable changes from false to true
	bool Deleted = false;

	// Default constructor
	Widget()
	{
		// Creates vector of elements
		Elements = new std::vector<Element *>();
	}

	// Default destructor
	~Widget()
	{
		for (uint32 i = 0; i < Elements->size(); i++)
		{
			delete (*Elements)[i];
			(*Elements)[i] = nullptr;
		}

		delete Elements;
		Elements = nullptr;
	}

	// Add element pointer to vector
	void AddElement(Element *element)
	{
		Elements->push_back(element);
	}

	// Removes element (I'll implement this later, maybe)
	void RemoveElement(Element *element)
	{

	}

};

class Manager
{
private:

	// Vector of widgets
	std::vector<Widget *> *Widgets;

public:

	// Just random variable
	// Check debugging stats how this variable changes from false to true
	bool Deleted = false;

	// Default constructor
	Manager()
	{
		// Creates vector of widgets
		Widgets = new std::vector<Widget *>();
	}

	// Default destructor
	~Manager()
	{
		// Deletes all the widgets from memory
		for (uint32 i = 0; i < Widgets->size(); i++)
		{
			delete (*Widgets)[i];
			(*Widgets)[i] = nullptr;
		}

		// Deletes widget vector from memory
		delete Widgets;
		Widgets = nullptr;
	}

	// Add widget pointer to vector
	void AddWidget(Widget *widget)
	{
		Widgets->push_back(widget);
	}

	// Removes widget (I'll implement this later, maybe)
	void RemoveWidget(Widget *widget)
	{

	}

};

int main()
{
	Manager *manager = new Manager();

	Widget *wid = new Widget();

	{
		// Widget stores/holds 2 elements
		wid->AddElement(new Text());
		wid->AddElement(new Line());

		// Manager stores/holds widget
		manager->AddWidget(wid);
	}

	MSG("Deleting");

	delete manager;
	manager = 0;

	if (wid)
	{
		if (wid->Deleted)
		{
			//wid = nullptr;
			OutputDebugString("LALA\n");
			MSG("LALA");
		}
	}

	MSG("Done!");
	
	_getwch();

	return 0;
}

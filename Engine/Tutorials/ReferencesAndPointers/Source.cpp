#include <map>
#include <iostream>
#include <string>

typedef const char* Name;

using std::map;
using std::string;
using std::cout;
using std::endl;
using std::cin;

map<Name, int> Map;

class Abstract
{
public:

	virtual void Print()
	{
		cout << "Something Abstract" << endl;
	}
};

class BA1 : public Abstract
{
public:

	BA1() {}
	~BA1() {}

	void Print()
	{
		cout << "BA1 -- prints this message!" << endl;
	}
};

class BA2 : public Abstract
{
public:

	BA2() {}
	~BA2() {}

	void Print()
	{
		cout << "BA2 == prints this message!" << endl;
	}
};

struct PrintableObject
{
	string CustomMessage;

	PrintableObject()
	{
		CustomMessage = "NONE";
	}

	PrintableObject(string Message)
	{
		this->CustomMessage = Message;
	}

	void Print()
	{
		std::cout << "Iam the object - " << CustomMessage << std::endl;
	}

};

class Temp
{
private:

	// First local map
	map<Name, PrintableObject> LocalMap;

	// Second local map
	map<Name, Abstract> AbstractMap;


public:

	// Constructor
	Temp();

	// Destructor
	~Temp();

	// Add element
	void
		AddElement(Name name, PrintableObject &value),
		AddElement(Name name, Abstract &value);

	// Get value by name
	PrintableObject *GetValuePrintableObject(Name name);
	Abstract *GetValueAbstract(Name name);

	// Remove element
	void
		RemoveElementPrintableObject(Name name),
		RemoveElementAbstract(Name name);

	// Print all map
	void PrintAll()
	{
		if (LocalMap.size())
		{
			cout << "== Printing LocalMap map" << endl;

			for (auto &item : LocalMap)
			{
				cout << item.first << endl;
				item.second.Print();
				cout << endl;
			}
		}

		if (AbstractMap.size())
		{
			cout << "== Printing AbstractMap map" << endl;

			for (auto &item : AbstractMap)
			{
				cout << item.first << endl;
				item.second.Print();
				cout << endl;
			}
		}
	}

};

int main()
{
	cout << "Test number [ 1 , 2 , 3 , 4 ]: ";
	int testNumber = -1;
	cin >> testNumber;

	if (testNumber == 1)
	{
		cout << "#############################################" << endl;

		Map.insert({ "aaa", 1 });
		Map.insert({ "bbb", 2 });
		Map.insert({ "ccc", 3 });

		cout << "Printing map" << endl;
		for (auto &item : Map)
		{
			cout << item.first << "\t" << item.second << endl;
		}
	}

	if (testNumber == 2)
	{
		cout << "#############################################" << endl;

		Temp temp;

		temp.AddElement("Test", PrintableObject());
		temp.AddElement("ABC", PrintableObject("ABC"));
		temp.AddElement("lel", PrintableObject("some object"));

		temp.PrintAll();

		cout << "== Printing some value" << endl;
		auto value = temp.GetValuePrintableObject("ABC");
		if (value) value->Print();
		cout << endl;

		temp.RemoveElementPrintableObject("Test");

		temp.PrintAll();
		cout << endl;

		value = temp.GetValuePrintableObject("asdasdasda");
		cout << value << endl;
	}

	if (testNumber == 3)
	{
		cout << "#############################################" << endl;

		Temp temp;

		temp.AddElement("abs", Abstract());
		temp.AddElement("ba1", BA1());
		temp.AddElement("ba2", BA2());

		temp.PrintAll();

		cout << "== Printing some value" << endl;
		auto value = temp.GetValueAbstract("abs");
		if (value) value->Print();
		cout << endl;

		temp.RemoveElementAbstract("ba1");

		temp.PrintAll();
		cout << endl;

		value = temp.GetValueAbstract("asdasdasda");
		cout << value << endl;
	}

	if (testNumber == 4)
	{
		cout << "#############################################" << endl;
		cout << "Random stuff with convertation (static_cast<T>)" << endl;

		Temp temp;

		temp.AddElement("abs", Abstract());
		temp.AddElement("ba1", BA1());
		temp.AddElement("ba2", BA2());

		BA1 *value1 = static_cast<BA1*>(temp.GetValueAbstract("ba1"));
		if (value1) value1->Print();

		BA2 * value2 = static_cast<BA2*>(temp.GetValueAbstract("ba2"));
		if (value2) value2->Print();

	}

	_getwch();
	return 0;
}

Temp::Temp()
{

}

Temp::~Temp()
{

}

void Temp::RemoveElementPrintableObject(Name name)
{
	auto iterator = LocalMap.find(name);

	if (iterator != LocalMap.end())
	{
		iterator = LocalMap.erase(iterator);
	}
}

void Temp::RemoveElementAbstract(Name name)
{
	auto iterator = AbstractMap.find(name);

	if (iterator != AbstractMap.end())
	{
		iterator = AbstractMap.erase(iterator);
	}
}

void Temp::AddElement(Name name, PrintableObject &value)
{
	LocalMap[name] = value;
}

void Temp::AddElement(Name name, Abstract &value)
{
	this->AbstractMap[name] = value;
}

Abstract *Temp::GetValueAbstract(Name name)
{
	auto iterator = AbstractMap.find(name);

	if (iterator != AbstractMap.end())
	{
		return &iterator->second;
	}

	return nullptr;
}


PrintableObject *Temp::GetValuePrintableObject(Name name)
{
	auto iterator = LocalMap.find(name);

	if (iterator != LocalMap.end())
	{
		return &iterator->second;
	}

	return nullptr;
}

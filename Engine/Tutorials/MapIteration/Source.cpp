#include <map>
#include <iostream>

using std::map;
using std::cout;
using std::endl;

map<int, double*> Map;

void printmap(map<int, double*> &Map)
{
	cout << "Iterating through map(print)" << endl;
	for (auto &item : Map)
	{
		cout << item.first << endl;
		cout << *item.second << endl;
		cout << endl;
	}
}

void printmap1(map<int, double*> &Map)
{
	cout << "Iterating through map(print1)" << endl;
	for (auto iter = Map.begin(); iter != Map.end(); iter++)
	{
		cout << iter->first << endl;
		cout << *iter->second << endl;
		cout << endl;
	}
}

void printmap2(map<int, double*> &Map)
{
	cout << "Iterating through map(print2)" << endl;
	for (auto iter = Map.begin(); iter != Map.end(); ++iter)
	{
		cout << iter->first << endl;
		cout << *iter->second << endl;
		cout << endl;
	}
}

void deleteValue(map<int, double*> &Map, double value)
{
	cout << "Iterating through map (delete)" << endl;

	for (auto item = Map.begin(); item != Map.end(); ++item)
	{
		if (*item->second == value)
		{
			delete item->second;
			item->second = nullptr;
			item = Map.erase(item);
		}
	}
}

void arrayTest()
{
	int a[5] = { 1,2,3,4,5 };

	cout << "Testing array and for loop1" << endl;
	for (int i = 0; i < 5; ++i)
	{
		cout << a[i] << endl;
	}

	cout << "Testing array and for loop2" << endl;
	for (int i = 0; i < 5; i++)
	{
		cout << a[i] << endl;
	}
}

int main()
{
	Map[0] = new double(5);
	Map[1] = new double(4);
	Map[2] = new double(3);
	Map[3] = new double(2);

	printmap1(Map);
	printmap2(Map);

	printmap(Map);
	deleteValue(Map, 3.0);
	printmap(Map);

	arrayTest();

	cout << "Done" << endl;
	_getwch();
	return 0;
}

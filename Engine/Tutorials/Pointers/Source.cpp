#include <iostream>
using std::cout;
using std::endl;

struct MyStruct
{
	int *ptr1 = nullptr;
	int **ptr2 = nullptr;

	MyStruct(int *ptr1)
	{
		this->ptr1 = ptr1;
		this->ptr2 = nullptr;
	}

	MyStruct(int **ptr2)
	{
		this->ptr1 = nullptr;
		this->ptr2 = ptr2;
	}

	MyStruct(int *ptr1, int **ptr2)
	{
		this->ptr1 = ptr1;
		this->ptr2 = ptr2;
	}

};

void Check(int **ptr1, int *ptr2)
{
	cout << endl;

	if (ptr1)
	{
		cout << "Pointer A:" << endl;

		cout << "PTR1 \t" << ptr1 << endl;
		cout << "PTR1&\t" << &ptr1 << endl;
		cout << "PTR1*\t" << *ptr1 << endl;
		cout << "PTR1**\t" << **ptr1 << endl;
	}

	cout << endl;

	if (ptr2)
	{
		cout << "Pointer B:" << endl;

		cout << "PTR2 \t" << ptr2 << endl;
		cout << "PTR2*\t" << *ptr2 << endl;
		cout << "PTR2&\t" << &ptr2 << endl;
	}
}

int main()
{
	{
		cout << "Pirma dalis labiau skirta ziureti visu pointeriu reiksmes ir kaip jos veikia: " << endl;

		int *a = new int(5);
		int *b = new int(4);

		cout << "Pointer A:" << endl;

		cout << "A    \t" << a << endl;
		cout << "A*   \t" << *a << endl;
		cout << "A&   \t" << &a << endl;

		cout << endl;

		cout << "Pointer B:" << endl;

		cout << "B    \t" << b << endl;
		cout << "B*   \t" << *b << endl;
		cout << "B&   \t" << &b << endl;

		Check(&a, b);

		delete a;
		delete b;

		a = nullptr;
		b = nullptr;
	}
	cout << endl;

	cout << "################################################" << endl;

	{
		cout << "Nustatyta, kad bus Pointeris 'q'" << endl;
		int *q = nullptr;

		cout << "Sukuriama struktura 'str1' - persiunciamas parametras tiesiog 'q'" << endl;
		MyStruct *str1 = new MyStruct(q);
		cout << "Patikriname ar strukturoje 'str1' yra reiksme PRIES 'q' sukurima: " << ((str1->ptr1) ? "yra" : "nera") << endl;

		q = new int(111);
		cout << "Patikriname ar strukturoje 'str1' yra reiksme PO 'q' sukurimo: " << ((str1->ptr1) ? "yra" : "nera") << endl;

		cout << "Istriname senus kintamuosius, darome \"Kitaip\"" << endl;
		delete str1;
		str1 = nullptr;
		delete q;
		q = nullptr;

		cout << endl << "Double pointer:" << endl;
		cout << "Kuriame nauja struktura 'str1' - persiunciame parametra '&q'" << endl;
		str1 = new MyStruct(&q);

		cout << "Patikriname ar strukturoje 'str1' yra pointerio reiksme PRIES 'q' sukurima: " 
			<< ((str1->ptr2) ? "pointeris yra" : "pointerio nera") 
			<< endl;

		cout << "Patikriname ar strukturoje 'str1' yra pointerio reiksme i kuria pointina PRIES 'q' sukurima: " 
			<< (*(str1->ptr2) ? "reiksmes 'q' yra" : "reiksmes 'q' nera")
			<< endl;

		q = new int(444);

		cout << "Patikriname ar strukturoje 'str1' yra pointerio reiksme PO 'q' sukurima: " 
			<< ((str1->ptr2) ? "pointeris yra" : "pointerio nera") 
			<< endl;

		cout << "Patikriname ar strukturoje 'str1' yra pointerio reiksme i kuria pointina PO 'q' sukurima: " 
			<< (*(str1->ptr2) ? "reiksmes 'q' yra" : "reiksmes 'q' nera") 
			<< endl;

		delete str1;
		delete q;
		str1 = nullptr;
		q = nullptr;
	}

	_getwch();

	return 0;
}

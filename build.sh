#!/bin/bash

echo "#Collecting all files..."

files=(Engine/EngineCore/*.cpp)
mainf=(Engine/EngineCore/*main*)
count=${#files[@]}
cver="-std=c++14"
echo "#Found files (.cpp): $count"
echo

definitions="-DDEBUG -DOS_LINUX"
echo "#Defines for building: $definitions"
libraries="-LSFML-2.4.2/lib -ISFML-2.4.2/include -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lpthread"
echo "#Libraries for building: $libraries"
echo

buildFailed="false"

for i in "${files[@]}"; do
    #newname=${i/".cpp"/}.o
    fname=${i/"Engine/EngineCore/"/}
    fname=${fname/".cpp"/}
    fname="Engine/Build/Game/$fname.o"
    echo "#Compiling file...: $i ---> $fname"
    if ! g++ $cver -g -c -pipe $i $definitions $libraries -o $fname ; then
        exit 1
    fi
done

echo "#Making final step..."
g++ $cver -pipe Engine/Build/Game/*.o $definitions $libraries -o Engine/Build/Game/Game-Linux-Debug.out

echo "#Done!"
